<?php

require_once __DIR__ . "/../api/commands/awqot_ensure_upgrade.php";
require_once __DIR__ . "/../api/tools/database.php";
require_once __DIR__ . "/../api/tools/is_online.php";

// ----- CONTROLLER -----

if (isset($_POST["action"])) switch ($_POST["action"]) {
  case "scheduling_compute":
  break;
}

if ($_SERVER["REQUEST_METHOD"] === "POST") {
  require_once __DIR__ . "/../api/commands/scheduling_compute.php";
  header("Location: {$_SERVER["REQUEST_URI"]}");
  exit();
}

// ----- MODEL -----

$is_connect_internet = is_online();

$rpi_serial_number = exec("cat /proc/cpuinfo | grep Serial | awk ' {print $3}'");
$product_key = get_configuration("product_key");

$is_save_mode = strpos(
  file_get_contents("/home/pi/.bash_profile"),
  "# php-cgi /home/pi/awqot-bel/api/commands/init.php"
) !== false;

// ----- VIEW -----

require_once __DIR__ . "/../components/awqot_admin.php";
require_once __DIR__ . "/../components/basic_style.php";
require_once __DIR__ . "/../components/box.php";
require_once __DIR__ . "/../components/button.php";
require_once __DIR__ . "/../components/button_group.php";
require_once __DIR__ . "/../components/event.php";
require_once __DIR__ . "/../components/field.php";
require_once __DIR__ . "/../components/head.php";
require_once __DIR__ . "/../components/icon.php";
require_once __DIR__ . "/../components/modal.php";
require_once __DIR__ . "/../components/notification.php";
require_once __DIR__ . "/../components/page.php";
require_once __DIR__ . "/../components/sidebar.php";

?><!DOCTYPE html>

<html lang="id">

<head>
  <title>Sistem - Awqot</title>
  <?php publish("head"); ?>
  <style>
  #awqot_id {
    box-sizing: border-box;
    padding: 1rem;
    background-color: #eaeaea;
    text-align: center;
  }

  .fancy-checkbox input[type="checkbox"] {
    position: relative;
    width: 48px;
    height: 24px;
    -webkit-appearance: none;
    background: #c6c6c6;
    outline: none;
    cursor: pointer;
    border-radius: 12px;
    box-shadow: inset 0 0 5px rgba(0, 0, 0, 0.2);
    transition: background 200ms linear;
  }

  .fancy-checkbox input:checked[type="checkbox"] {
    background: #03a9f4;
  }

  .fancy-checkbox input[type="checkbox"]::before {
    content: "";
    position: absolute;
    width: 24px;
    height: 24px;
    border-radius: 12px;
    top: 0;
    left: 0;
    background: #fff;
    transform: scale(1.1);
    box-shadow: 0 2px 5px rgba(0, 0, 0, 0.2);
    transition: left 200ms linear;
  }

  .fancy-checkbox input:checked[type="checkbox"]::before {
    left: 24px;
  }
  </style>
</head>

<body>
  <?php publish("body"); ?>
  <div id="app" class="page" style="padding-top: 3.5rem;">
    <header class="page-header">
      <div class="page-header-row">
        <div class="page-header-left">
          <button type="button" class="button" onclick="ev.publish('sidebar:open');">
            <span class="icon">
              <?php include __DIR__ . "/../static/icons/round-menu-24px.svg"; ?>
            </span>
          </button>
          <h1 class="page-title">Sistem Awqot</h1>
        </div>
      </div>
    </header>

    <main class="page-content padded">
      <section class="box">
        <div class="box-content" style="padding-top: .5rem;">
          <p>
            <button type="button" onclick="window.location.pathname = '/pages/register.php'" class="button primary solid" style="display: inline-flex;" <?= $is_connect_internet ? "" : "disabled" ?>>
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-assignment-24px.svg" ?>
              </span>
              <span class="text">Data Pengguna</span>
            </button>
          </p>
          <p>Klik tombol ini untuk membuka halaman formulir pendataan pengguna Awqot. Identitas yang terdaftar akan mendapat akses bantuan melalui group WhatsApp.</p>
          <hr>

          <form class="button-group" method="POST">
            <input type="hidden" name="action" value="scheduling_compute">
            <button type="submit" class="button primary solid">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-autorenew-24px.svg" ?>
              </span>
              <span class="text">Jadwalkan Ulang</span>
            </button>
          </form>
          <p>Menjadwalkan ulang materi untuk hari ini. Gunakan tombol ini apabila anda ingin memastikan jadwal materi sesuai atau saat waktu di alat Awqot diperbarui menggunakan internet.</p>
          <hr>

          <div class="button-group">
            <button type="button" class="button primary solid" onclick="ev.publish('modal:open', { dialog: awqot_update_register_notice });" <?= $is_connect_internet ? "" : "disabled" ?>>
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-system_update-24px.svg" ?>
              </span>
              <span class="text">Update Otomatis</span>
            </button>
          </div>
          <p>Update apliaksi awqot secara otomatis. Pastikan bahwa anda terhubung melalui kabel USB dan smartphone anda memiliki jaringan internet.</p>
          <hr>

          <div class="button-group">
            <button type="button" class="button primary solid" onclick="ev.publish('modal:open', { dialog: awqot_remote })" <?= $is_connect_internet ? "" : "disabled" ?>>
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-settings_remote-24px.svg" ?>
              </span>
              <span class="text">Bantuan Jarak Jauh</span>
            </button>
          </div>
          <p>Berikan akses kepada pengembang untuk mengendalikan alat Awqot secara jarak jauh. Tombol ini dibutuhkan apabila terjadi kendala pada alat Awqot dan ingin memberikan akses kendali kepada pengembang.</p>
          <hr>

          <form id="save_mode_form" class="button-group" method="POST">
            <div
              style="
                display: flex;
                align-items: center;
                justify-content: space-between;
                width: 100%;
                padding: 0 .2rem;
              "
            >
              <label for="save_mode_input" style="font-size: 1.125rem; font-weight: 500;">Mode Aman</label>
              <div class="fancy-checkbox">
                <input id="save_mode_input" type="checkbox" name="save_mode" value="active" onchange="saveModeChange()" <?= $is_save_mode ? "checked" : "" ?>>
              </div>
            </div>
          </form>
          <p>Apabila mode aman aktif maka otomatisasi tidak akan berjalan saat pertama kali Awqot dinyalakan. Anda harus membuka aplikasi untuk menjalankan otomatisasi. Hal ini hanya perlu dilakukan <strong>sekali saja</strong>. Setelah otomatisasi berjalan maka akan berjalan terus setiap harinya.</p>
          <p>Mode aman bertujuan untuk memastikan agar tidak terjadi kesalahan pada waktu di Awqot. Jadi fitur ini sangat bermanfaat untuk anda yang khawatir jika Awqot tidak berjalan pada waktu yang tepat.</p>
        </div>
      </section>
      <section class="box">
        <div class="box-header">
          <h3 class="box-title">Versi Awqot</h3>
        </div>
        <div class="box-content">
          <pre
            style="margin: 1rem; padding: 1rem; background-color: #f2f2f2; overflow-x: auto;"
          ><?= htmlspecialchars(shell_exec("cd " . realpath(__DIR__ . "/../") . " && git show | head -3")) ?></pre>
        </div>
      </section>
      <section class="box">
        <div class="box-header">
          <h3 class="box-title">Pengembang</h3>
        </div>
        <div class="box-content">
          <p><strong>Hardware</strong></p>
          <p>Muhammad Jamhari, S.Ag<br>+62 857 4356 7723</p>
          <p><br><strong>Software</strong></p>
          <p>Faisal Hakim<br>+62 821 4782 4783</p>
        </div>
      </section>
    </main>

    <!-- AWQOT_UPDATE_REGISTER_NOTICE -->
    <dialog id="awqot_update_register_notice" class="modal">
      <div class="modal-overlay"></div>
      <form
        id="register_form"
        class="modal-shell"
        enctype="multipart/form-data"
        action="https://admin.awqot.com/api/registerAwqotOwnership"
        onsubmit="registerSubmit(this, event);"
      >
        <div class="modal-content">
          <input type="hidden" name="rpiSerialNumber" value="<?= $rpi_serial_number ?>">
          <input type="hidden" name="productKey" value="<?= $product_key ?>">
          <input type="hidden" name="organizationType" value="bel">
          <div class="field">
            <div class="field-label">
              <label for="register_name">Nama Pengurus</label>
            </div>
            <div class="field-input">
              <input
                id="register_name"
                type="text"
                name="name"
                autocomplete="name"
                placeholder="nama lengkap"
                required
              >
            </div>
          </div>
          <div class="field">
            <div class="field-label">
              <label for="register_phoneNumber">Nomor Telepon Pengurus</label>
            </div>
            <div class="field-input">
              <input
                id="register_phoneNumber"
                type="tel"
                name="phoneNumber"
                autocomplete="tel"
                placeholder="nomor telepon atau nomor whatsapp"
                required
              >
            </div>
            <p class="field-info">Disarankan untuk mencantumkan nomor yang aktif WhatsApp.</p>
          </div>
          <div class="field">
            <div class="field-label">
              <label for="register_organizationName">Nama Institusi</label>
            </div>
            <div class="field-input">
              <input
                id="register_organizationName"
                type="text"
                name="organizationName"
                placeholder="nama institusi yang dipasang Awqot"
                required
              >
            </div>
          </div>
          <div class="field">
            <div class="field-label">
              <label for="register_address">Alamat Institusi</label>
            </div>
            <div class="field-input">
              <textarea
                id="register_address"
                type="text"
                name="address"
                autocomplete="shipping street-address"
                placeholder="alamat institusi tempat dipasang Awqot"
                required
              ></textarea>
            </div>
          </div>
          <div class="field">
            <div class="field-label">
              <label for="register_photo">Foto Institusi</label>
            </div>
            <div class="field-input">
              <input id="register_photo" type="file" name="photo" accept="image/*">
            </div>
          </div>
        </div>
        <footer class="modal-footer">
          <div class="modal-footer-left">
            <button
              type="button"
              class="button secondary"
              onclick="ev.publish('modal:close', { dialog: awqot_update_register_notice }); ev.publish('modal:open', { dialog: awqot_update });"
            >
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-arrow_right_alt-24px.svg" ?>
              </span>
              <span class="text">Lewati</span>
            </button>
          </div>
          <div class="modal-footer-right">
            <button type="submit" class="button primary">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-save-24px.svg" ?>
              </span>
              <span class="text">Perbarui</span>
            </button>
          </div>
        </footer>
      </form>
    </dialog>
    <!-- /AWQOT_UPDATE_REGISTER_NOTICE -->

    <!-- AWQOT_UPDATE -->
    <dialog id="awqot_update" class="modal">
      <div class="modal-overlay"></div>
      <div class="modal-shell">
        <div class="modal-content">
          <p>Update aplikasi Awqot membutuhkan koneksi internet. Pastikan anda terhubung dengan awqot melalui USB dan pastikan ponsel anda memiliki jaringan internet.</p>
          <p>Update memerlukan waktu kurang lebih 1 - 5 menit. Harap tunggu hingga proses selesai.</p>
        </div>
        <footer class="modal-footer">
          <div class="modal-footer-left">
            <button
              type="button"
              class="button secondary"
              onclick="ev.publish('modal:close', { dialog: awqot_update });"
            >
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-close-24px.svg" ?>
              </span>
              <span class="text">Batal</span>
            </button>
          </div>
          <div class="modal-footer-right">
            <a
              href="/api/awqot_upgrade.php"
              class="button primary"
              onclick="this.querySelector('.text').textContent = 'Sedang Memperbarui...';this.disabled = true;"
            >
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-system_update-24px.svg" ?>
              </span>
              <span class="text">Update Sekarang</span>
            </a>
          </div>
        </footer>
      </div>
    </dialog>
    <!-- /AWQOT_UPDATE -->

    <!-- AWQOT_REMOTE -->
    <dialog id="awqot_remote" class="modal">
      <div class="modal-overlay"></div>
      <div class="modal-shell">
        <div class="modal-content">
          <p>Saat ini perangkat Awqot anda membuka akses kepada pengembang untuk perawatan.</p>
          <p>Harap jangan melepas kabel USB apabila proses belum selesai!</p>
          <p>Tunjukan kode unik ini kepada pengembang agar pengembang dapat mengakses alat Awqot anda.</p>
          <pre id="awqot_id"><?= $product_key ?></pre>
          <div class="button-group align-center">
            <a
              href="https://wa.me/6282147824783?text=https://admin.awqot.com/AwqotTasks/<?= $product_key ?>?tab=undone"
              class="button primary"
              target="_blank"
            >
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-send-24px.svg" ?>
              </span>
              <span class="text">Kirim Kode Melalui WA</span>
            </a>
          </div>
        </div>
        <footer class="modal-footer" style="justify-content: center;">
          <div class="modal-footer-right">
            <button class="button danger" onclick="ev.publish('modal:close', { dialog: awqot_remote })">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-stop-24px.svg" ?>
              </span>
              <span class="text">Hentikan Bantuan</span>
            </button>
          </div>
        </footer>
      </div>
    </dialog>
    <!-- /AWQOT_REMOTE -->
  </div>
<?php
echo "<script>";
require_once __DIR__ . "/about.js";
echo "</script>"; 
?>
</body>

</html>
