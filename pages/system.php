<?php

require_once __DIR__ . "/../api/commands/awqot_ensure_upgrade.php";
require_once __DIR__ . "/../api/tools/database.php";
require_once __DIR__ . "/../api/tools/configuration.php";

// ----- CONTROLLER -----

if (isset($_POST["action"])) switch ($_POST["action"]) {
  case "awqot_set":
  set_configuration("awqot_mode", $_POST["awqot_mode"]);
  $new_time = ((int) strtotime($_POST["test_awqot_time"]));
  exec("sudo date +%s -s @{$new_time}");
  exec("sudo hwclock -w");
  if ($_POST["awqot"] === "masjid") {
    shell_exec("sudo rm -rf /etc/apache2/sites-enabled/awqot.conf");
    shell_exec("sudo ln -s /etc/apache2/sites-available/awqot-masjid.conf /etc/apache2/sites-enabled/awqot.conf");
    shell_exec("sed -i 's/bel/masjid/g' /home/pi/.bash_profile");
    shell_exec("php-cgi " . __DIR__ . "/../api/commands/scheduling_stop.php");
    async_exec("sleep 1 && sudo systemctl restart apache2");
    ?><!DOCTYPE html>
<title>Memproses...</title>
<p>tunggu beberapa detik...</p>
<script>
setTimeout(function () {
  var data = new FormData();
  data.append('action', 'awqot_set');
  data.append('awqot', 'masjid');
  data.append('awqot_mode', '<?= get_configuration("awqot_mode") ?>');
  var request = new XMLHttpRequest();
  request.open('POST', '/pages/system.php');
  request.addEventListener('readystatechange', function () {
    if (request.readyState === request.DONE) {
      setTimeout(function () {
        window.location.pathname = '/';
      }, 1000);
    }
  });
  request.send(data);
}, 5000);
</script><?php
    exit();
  }
  break;
}

if ($_SERVER["REQUEST_METHOD"] === "POST") {
  require_once __DIR__ . "/../api/commands/scheduling_compute.php";
  header("Location: {$_SERVER["REQUEST_URI"]}");
  exit();
}

// ----- MODEL -----

$awqot_mode = get_configuration("awqot_mode");

// ----- VIEW -----

require_once __DIR__ . "/../components/basic_style.php";
require_once __DIR__ . "/../components/box.php";
require_once __DIR__ . "/../components/button.php";
require_once __DIR__ . "/../components/button_group.php";
require_once __DIR__ . "/../components/field.php";
require_once __DIR__ . "/../components/head.php";
require_once __DIR__ . "/../components/icon.php";
require_once __DIR__ . "/../components/notification.php";
require_once __DIR__ . "/../components/page.php";
require_once __DIR__ . "/../components/sidebar.php";

?><!DOCTYPE html>

<html lang="id">

<head>
  <title>Sistem Awqot</title>
  <?php publish("head"); ?>
</head>

<body>
  <?php publish("body"); ?>

  <div id="app" class="page" style="padding-top: 3.5rem;">
    <header class="page-header">
      <div class="page-header-row">
        <div class="page-header-left">
          <button type="button" class="button" onclick="ev.publish('sidebar:open');">
            <span class="icon">
              <?php include __DIR__ . "/../static/icons/round-menu-24px.svg"; ?>
            </span>
          </button>
          <h1 class="page-title">Sistem Awqot</h1>
        </div>
      </div>
    </header>

    <main class="page-content padded">
      <section class="box">
        <form class="box-content" method="post" onsubmit="systemSubmit(this, event);">

          <input type="hidden" name="action" value="awqot_set">

          <div class="field">
            <div class="field-label">
              <label for="awqot">Mode Awqot</label>
            </div>
            <div class="field-input">
              <select id="awqot" name="awqot">
                <option value="masjid">Masjid</option>
                <option value="bel" selected>Bel</option>
              </select>
            </div>
          </div>

          <div class="field">
            <div class="field-label">
              <label for="awqot_mode">Model Alat Awqot</label>
            </div>
            <div class="field-input">
              <select id="awqot_mode" name="awqot_mode">
                <option
                  value="default"
                  <?= $awqot_mode === "default" ? "selected" : "" ?>
                >Normal</option>
                <option
                  value="mini"
                  <?= $awqot_mode === "mini" ? "selected" : "" ?>
                >Mini</option>
              </select>
            </div>
          </div>

          <div class="field">
            <div class="field-label">
              <label for="test_awqot_time">Jam Awqot (Untuk Uji Coba)</label>
            </div>
            <div class="field-input">
              <input
                id="test_awqot_time"
                type="time"
                name="test_awqot_time"
                value="<?= date("H:i", time()); ?>"
              >
            </div>
          </div>

          <div class="button-group align-right">
            <button type="submit" class="button primary solid">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-save-24px.svg" ?>
              </span>
              <span class="text">Simpan</span>
            </button>
          </div>

        </form>
      </section>
    </main>
  </div>

  <script>
    /**
     * @param {HTMLFormElement} form
     * @param {Event} event
     */
    function systemSubmit(form, event) {
      /** @type {HTMLButtonElement} */
      var submitButton = form.querySelector('button[type="submit"]');
      if (submitButton.disabled) {
        event.preventDefault();
      }
      submitButton.disabled = true;
    }
  </script>
</body>

</html>
