const audios = window.audios
  .map((audio) => {
    return {
      isNew: false,
      hash: audio.hash,
      filename: audio.filename,
      filetype: audio.filetype,
      duration: audio.duration,
    };
  });
const schedule = window.schedule;
const schedule_times = window.schedule_times
  .map((scheduleTime) => {
    if (scheduleTime.audios !== 'null') {
      scheduleTime.audios = JSON.parse(scheduleTime.audios)
        .map((audio_hash) => {
          return audios.find((audio) => audio.hash === audio_hash);
        });
    }
    else {
      scheduleTime.audios = [];
    }

    if (scheduleTime.relay_pins !== 'null') {
      scheduleTime.relayPins = JSON.parse(scheduleTime.relay_pins)
        .filter(function (pin) {
          return !!pin;
        })
        .map((pin) => {
          return pin.toString();
        });
    }
    else {
      scheduleTime.relayPins = [];
    }

    return scheduleTime;
  });

const days = [
  { value: 'ahad', label: 'Ahad' },
  { value: 'senin', label: 'Senin' },
  { value: 'selasa', label: 'Selasa' },
  { value: 'rabu', label: 'Rabu' },
  { value: 'kamis', label: 'Kamis' },
  { value: 'jumat', label: 'Jum\'at' },
  { value: 'sabtu', label: 'Sabtu' },
].map(({ value, label }, index) => {
  const id = index;
  const times = schedule_times.filter((time) => {
    return time.day === id;
  });
  return { id, value, label, times }
});

if (!location.hash) {
  const sparator = location.href.indexOf('#') === -1 ? '#' : '';
  location.replace(location.href + sparator + days[new Date().getDay()].value);
}

const app = new Vue({
  el: document.getElementById("app"),

  data() {
    return {
      days,
      audios,
      schedule,
      isSaving: false,
      isLoaded: false,
      isUploading: false,
      modalSchedule: false,
      modalScheduleDelete: false,
      modalScheduleTime: false,
      modalScheduleTimeDelete: false,
      modalLibrary: false,
      modalDuplicate: false,
      selectorKeyword: '',
      activeDayValue: days[0].value,
      selectedAudios: [],
      selectedDays: [],
      scheduleTime: {
        id: false,
        hour: null,
        minute: null,
        audios: [],
        relayPins: window.speakerPins,
      },
    };
  },

  computed: {
    isNewTime() {
      return !this.scheduleTime.id;
    },

    isEmpty() {
      return this.audios.length === 0;
    },

    isNotFound() {
      return !this.isEmpty && this.filteredAudios.length === 0;
    },

    today() {
      return this.days[new Date().getDay()];
    },

    activeDay() {
      return this.days.find((day) => day.value === this.activeDayValue);
    },

    filteredAudios() {
      const rx = new RegExp(this.selectorKeyword, 'i');
      return this.audios
        .filter((audio) => {
          return rx.test(audio.filename);
        });
    },
  },

  methods: {
    relayPinDisplay(pins) {
      var pinMap = [];
      pinMap[window.speakerPins[0]] = 'B1';
      pinMap[window.speakerPins[1]] = 'B2';
      pinMap[window.speakerPins[2]] = 'B3';
      var pinTexts = pins
        .map(function (pin) {
          return pinMap[pin];
        })
        .sort(function (text1, text2) {
          return text1.localeCompare(text2);
        });
      return pinTexts.join(', ');
      var lastPinText = pinTexts.pop();
      var pinDisplay = pinTexts.join(', ');
      if (pinTexts.length > 1) pinDisplay = pinDisplay + ',';
      if (lastPinText && pinDisplay) pinDisplay = pinDisplay + ' dan ' + lastPinText;
      else pinDisplay = lastPinText;
      return pinDisplay;
    },

    durationFormat(input) {
      input = parseInt(input, 10);
      const milliseconds = input % 1000;
      input = (input - milliseconds) / 1000;
      let seconds = (input % 60);
      let minutes = ((input - seconds) / 60);
      seconds = seconds === 0 ? '' : seconds + 'd';
      minutes = minutes === 0 ? '' : minutes + 'm';
      return minutes + (seconds ? seconds : '');
    },

    toDigit(number, numberOfDigit) {
      let string = number.toString();
      if (string.length < numberOfDigit) {
        for (let i = numberOfDigit - string.length; i--;) {
          string = '0' + string;
        }
      }
      return string;
    },

    openScheduleTime(time) {
      this.scheduleTime = time;
      this.modalScheduleTime = true;
    },

    uploadAudios() {
      this.isUploading = true;
      const request = new XMLHttpRequest();
      request.open('POST', '../api/audio_upload.php', true);
      request.addEventListener('readystatechange', () => {
        if (request.readyState === XMLHttpRequest.DONE) {
          if (request.status === 200) {
            const audios = JSON.parse(request.responseText)
              .map((audio) => {
                return {
                  isNew: true,
                  hash: audio.hash,
                  filename: audio.filename,
                  filetype: audio.filetype,
                  duration: audio.duration,
                };
              });
            Array.prototype.push.apply(audios, this.audios);
            this.audios = audios.filter((audio, index, audios) => {
              const firstIndex = audios.findIndex((_audio) => {
                return _audio.hash === audio.hash;
              });
              return firstIndex === index;
            });
            this.audios.filter((audio) => audio.isNew).forEach((audio) => {
              this.toggleSelectAudio(audio, 1);
            });
          } else alert(`MASALAH SERVER: ${request.responseText}`);
          this.isUploading = false;
        }
      });
      request.addEventListener('error', () => {
        alert(`MASALAH KONEKSI`);
        this.isUploading = false;
      })
      request.send(new FormData(this.$refs.audio_upload_form));
    },

    toggleSelectAudio(audio, forceState = -1) {
      const index = this.selectedAudios.indexOf(audio);
      if (index === -1 && forceState !== 0) {
        this.selectedAudios.push(audio);
      }
      else if (forceState !== 1) {
        this.selectedAudios.splice(index, 1);
      }
    },

    selectAudioDone() {
      this.modalLibrary = false;
      this.scheduleTime.audios.push(...this.selectedAudios);
      this.selectorKeyword = '';
      this.selectedAudios = [];
    },

    playAudio(audio) {
      ev.publish('materi_player:open', { audio_hash: audio.hash });
    },

    moveAudioPosition(audio, move) {
      const index = this.scheduleTime.audios.indexOf(audio);
      this.scheduleTime.audios.splice(index, 1);
      const destinationIndex = index + move;
      if (destinationIndex < 0) {
        this.scheduleTime.audios.splice(this.scheduleTime.audios.length, 0, audio);
      }
      else if (destinationIndex >= this.scheduleTime.audios.length) {
        this.scheduleTime.audios.splice(0, 0, audio);
      }
      else {
        this.scheduleTime.audios.splice(destinationIndex, 0, audio);
      }
    },

    toggleSelectDay(day) {
      const index = this.selectedDays.indexOf(day);
      if (index === -1) {
        this.selectedDays.push(day);
      }
      else {
        this.selectedDays.splice(index, 1);
      }
    },
  },

  watch: {
    modalLibrary(modalLibrary) {
      if (!modalLibrary) {
        this.selectorKeyword = '';
        this.selectedAudios = [];
      }
    },

    modalScheduleTime(modalScheduleTime) {
      if (!modalScheduleTime) {
        setTimeout(() => {
          this.scheduleTime = {
            id: false,
            hour: null,
            minute: null,
            audios: [],
            relays: window.speakerPins,
          };
        }, 200);
      }
    },
  },

  mounted() {
    this.$nextTick().then(() => {
      this.isLoaded = true;
    });
    this.activeDayValue = location.hash.slice(1);
    window.addEventListener('hashchange', () => {
      this.activeDayValue = location.hash.slice(1);
    });
    window.addEventListener('beforeunload', () => {
      this.isLoaded = false;
    });
    window.addEventListener('unload', () => {
      this.isLoaded = false;
    });
  },
});
