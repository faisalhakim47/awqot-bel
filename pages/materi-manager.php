<?php

require_once __DIR__ . "/../api/commands/awqot_ensure_upgrade.php";
require_once __DIR__ . "/../api/tools/database.php";

// ----- CONTROLLER -----

if (isset($_POST["action"])) switch ($_POST["action"]) {
  case "materi_edit":
  execute_update_sql("audios", [
    "filename" => [$_POST["filename"], PDO::PARAM_STR],
  ], [
    "hash" => [$_POST["hash"], PDO::PARAM_STR],
  ]);
  break;

  case "materi_delete":
  execute_delete_sql("audios", [
    "hash" => [$_POST["hash"], PDO::PARAM_STR],
  ]);
  exec("rm " . realpath(__DIR__ . "/../data/audios/" . escapeshellarg($_POST["hash"])));
  break;
}

if ($_SERVER["REQUEST_METHOD"] === "POST") {
  header("Location: {$_SERVER["REQUEST_URI"]}");
  exit();
}

// ----- MODEL -----

$audios = execute_sql("
  SELECT
    audios.hash AS hash,
    audios.filename AS filename,
    audios.duration AS duration
  FROM audios
")->fetchAll();

// ----- VIEW -----

require_once __DIR__ . "/../components/basic_style.php";
require_once __DIR__ . "/../components/button.php";
require_once __DIR__ . "/../components/button_fab.php";
require_once __DIR__ . "/../components/event.php";
require_once __DIR__ . "/../components/field.php";
require_once __DIR__ . "/../components/head.php";
require_once __DIR__ . "/../components/icon.php";
require_once __DIR__ . "/../components/list_view.php";
require_once __DIR__ . "/../components/menu.php";
require_once __DIR__ . "/../components/modal.php";
require_once __DIR__ . "/../components/materi_player.php";
require_once __DIR__ . "/../components/notification.php";
require_once __DIR__ . "/../components/page.php";
require_once __DIR__ . "/../components/sidebar.php";
require_once __DIR__ . "/../components/tag.php";

?><!DOCTYPE html>

<html lang="id">

<head>
  <title>Pengelola Materi - Awqot</title>
  <?php publish("head"); ?>
  <style>
  .schedule-controls {
    background-color: #FFFFFF;
    position: fixed;
    border-radius: .4rem;
    right: 1.5rem;
    top: 40%;
    top: calc(50% - 4.5rem);
    transform: translateX(10rem);
    transition-duration: 300ms;
    transition-property: transform;
    box-shadow: 0 8px 17px 2px rgba(0,0,0,0.14),
      0 3px 14px 2px rgba(0,0,0,0.12),
      0 5px 5px -3px rgba(0,0,0,0.2);
  }
  .audio-selected .schedule-controls {
    transform: translateX(0);
  }
  .schedule-controls .button {
    min-height: 3.5rem;
    border-radius: .4rem;
  }
  .schedule-controls .button:focus,
  .schedule-controls .button:hover {
    background-color: rgba(0, 0, 0, .05);
  }
  .schedule-controls .button .icon {
    padding: 1.25rem 1rem;
    margin: 0 !important;
  }
  .schedule-controls .button:disabled {
    cursor: not-allowed; 
  }
  .schedule-controls .button:disabled:hover {
    cursor: not-allowed; 
    background-color: rgba(0, 0, 0, 0);
  }
  .schedule-controls .button:disabled .icon svg path:last-child {
    fill: #aaa;
  }
  .audio-item.is-selected {
    background-color: #eee;
  }
  .audio-item.is-new .list-view-item-title {
    color: #388e3c;
  }
  </style>
</head>

<body>
  <?php publish("body"); ?>
  <div id="app" class="page" style="padding-top: 95px;">
    <header class="page-header">
      <div class="page-header-row">
        <div class="page-header-left">
          <button type="button" class="button" onclick="ev.publish('sidebar:open');">
            <div class="icon">
              <?php include __DIR__ . "/../static/icons/round-menu-24px.svg" ?>
            </div>
          </button>
          <h1 class="page-title">Pengelola Materi</h1>
        </div>
      </div>
      <div class="page-header-row">
        <div class="field">
          <div class="field-input">
            <input type="text" name="keyword" v-model="keyword" placeholder="pencarian">
          </div>
        </div>
      </div>
    </header>

    <main class="page-content" v-bind:class="{ 'audio-selected': selectedAudio }">
      <p v-if="filteredAudios.length === 0" class="notification">Materi tidak ditemukan</p>
      <ul v-else class="list-view">
        <li
          v-for="audio in filteredAudios"
          v-on:click="toggleAudio(audio)"
          v-bind:class="{ 'is-new': audio.isNew, 'is-selected': selectedAudio === audio }"
          class="audio-item list-view-item"
          style="cursor: pointer;"
        >
          <div class="list-view-item-row">
            <span class="list-view-item-title">{{ audio.filename }}</span>
          </div>
        </li>
      </ul>
      <form class="schedule-controls">
        <button
          type="button"
          class="button position-control"
          onclick="ev.publish('modal:open', { dialog: materi_edit });"
        >
          <div class="icon">
            <?php include __DIR__ . "/../static/icons/round-edit-24px.svg" ?>
          </div>
        </button>
        <button
          type="button"
          class="button primary position-control"
          onclick="ev.publish('materi_player:open', { audio_hash: app.selectedAudio.hash });"
        >
          <div class="icon">
            <?php include __DIR__ . "/../static/icons/round-play_arrow-24px.svg" ?>
          </div>
        </button>
        <button
          type="button"
          class="button danger"
          onclick="ev.publish('modal:open', { dialog: materi_delete });"
        >
          <div class="icon">
            <?php include __DIR__ . "/../static/icons/round-delete_forever-24px.svg" ?>
          </div>
        </button>
      </form>
    </main>

    <form
      ref="materi_manager_upload_form"
      action="/api/audio_upload.php"
      method="post"
      enctype="multipart/form-data"
      style="display: none;"
    >
      <input
        id="materi_manager_upload_input"
        type="file"
        name="audio_files[]"
        accept="audio/*"
        multiple="multiple"
        v-on:change="uploadMateri();"
      >
    </form>

    <button
      type="button"
      class="button-fab"
      onclick="document.getElementById('materi_manager_upload_input').click();"
    >
      <span class="icon">
        <?php include __DIR__ . "/../static/icons/round-cloud_upload-24px.svg" ?>
      </span>
      <span class="text">Upload Materi</span>
    </button>

    <!-- MATERI_EDIT -->
    <dialog v-if="selectedAudio" id="materi_edit" class="modal">
      <form class="modal-shell" method="post">
        <header class="modal-header">
          <div class="modal-header-row">
            <div class="modal-header-left">
              <h4 class="modal-title">Edit Materi</h4>
            </div>
          </div>
        </header>
        <div class="modal-content">
          <input type="hidden" name="action" value="materi_edit">
          <input type="hidden" name="hash" v-bind:value="selectedAudio.hash">
          <div class="field">
            <div class="field-label">
              <label class="label">Nama Materi</label>
            </div>
            <div class="field-input">
              <input
                ref="filename"
                class="input"
                type="text"
                name="filename"
                v-model="selectedAudio.filename"
                required
              >
            </div>
          </div>
        </div>
        <footer class="modal-footer">
          <div class="modal-footer-left">
            <button
              type="button"
              class="button"
              onclick="ev.publish('modal:close', { dialog: materi_edit });"
            >
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-close-24px.svg" ?>
              </span>
              <span class="text">Batal</span>
            </button>
          </div>
          <div class="modal-footer-right">
            <button type="submit" class="button primary">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-save-24px.svg" ?>
              </span>
              <span class="text">Simpan</span>
            </button>
          </div>
        </form>
      </footer>
    </dialog>
    <!-- /MATERI_EDIT -->

    <!-- MATERI_DELETE -->
    <dialog v-if="selectedAudio" id="materi_delete" class="modal">
      <form class="modal-shell" method="post">
        <div class="modal-content">
          <input type="hidden" name="action" value="materi_delete">
          <input type="hidden" name="hash" v-bind:value="selectedAudio.hash">
          <p>Apakah anda yakin menghapus materi "{{ selectedAudio.filename }}"?</p>
        </div>
        <footer class="modal-footer">
          <div class="modal-footer-left">
            <button
              type="button"
              class="button secondary"
              onclick="ev.publish('modal:close', { dialog: materi_delete });"
            >
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-close-24px.svg" ?>
              </span>
              <span class="text">Batal</span>
            </button>
          </div>
          <div class="modal-footer-right">
            <button type="submit" class="button danger">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-delete_forever-24px.svg" ?>
              </span>
              <span class="text">Hapus</span>
            </button>
          </div>
        </form>
      </footer>
    </dialog>
    <!-- /MATERI_DELETE -->
  </div>

  <script src="/static/scripts/vue.min.js"></script>
  <script>
  var app = new Vue({
    el: document.getElementById('app'),
    data() {
      return {
        selectedAudio: null,
        audios: <?= json_encode($audios) ?>.map(function (audio) {
          return {
            hash: audio.hash,
            filename: audio.filename,
            duration: audio.duration,
            isNew: false,
          };
        }),
        keyword: '',
      };
    },
    computed: {
      filteredAudios: function filteredAudios() {
        var its = this;
        return this.audios.filter(function (audio) {
          return audio.filename.indexOf(its.keyword) !== -1;
        }).sort(function(one, two) {
          if (one.filename < two.filename) { return -1; }
          if (one.filename > two.filename) { return 1; }
          return 0;
        });
      },
    },
    methods: {
      toggleAudio: function toggleAudio(audio) {
        if (this.selectedAudio === audio) this.selectedAudio = null;
        else this.selectedAudio = audio;
      },
      uploadMateri: function uploadMateri() {
        var its = this;
        var request = new XMLHttpRequest();
        request.open('POST', '/api/audio_upload.php', true);
        request.addEventListener('readystatechange', function () {
          if (request.readyState === XMLHttpRequest.DONE) {
            if (request.status === 200) {
              JSON.parse(request.responseText).forEach(function (audio) {
                its.audios.unshift({
                  hash: audio.hash,
                  filename: audio.filename,
                  duration: audio.duration,
                  isNew: true,
                });
              });
            } else alert(`MASALAH SERVER: ${request.responseText}`);
            ev.publish('materi_selector:audio_upload_done');
          }
        });
        request.addEventListener('error', function () {
          alert(`MASALAH KONEKSI`);
        });
        request.send(new FormData(its.$refs.materi_manager_upload_form));
      },
    },
    mounted: function mounted() {
      var its = this;
      var theHTML = document.getElementsByTagName('html').item(0);
      window.addEventListener('click', function (event) {
        if (event.target === theHTML) {
          its.selectedAudio = null;
        }
      });
    },
  });
  </script>
</body>

</html>
