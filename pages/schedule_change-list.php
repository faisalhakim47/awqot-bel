<?php

require_once __DIR__ . "/../api/commands/awqot_ensure_upgrade.php";
require_once __DIR__ . "/../api/tools/database.php";

// ----- CONTROLLER -----

if (isset($_POST["action"])) switch ($_POST["action"]) {
  case "schedule_change_create":
  $schedule_change_id = execute_insert_sql("schedule_changes", [
    "calendar" => [$_POST["calendar"], PDO::PARAM_STR],
    "month" => [$_POST["month"], PDO::PARAM_INT],
    "date" => [$_POST["date"], PDO::PARAM_INT],
    "schedule_id" => [$_POST["schedule_id"], PDO::PARAM_INT],
  ]);
  header("Location: /pages/schedule_change-list.php");
  exit();

  case "schedule_change_delete":
  execute_delete_sql("schedule_changes", [
    "id" => [$_POST["schedule_change_id"], PDO::PARAM_INT],
  ]);
  header("Location: /pages/schedule_change-list.php");
  exit();
}

// ----- MODEL -----

$hijriah_months = [
  "Muharam",
  "Safar",
  "Rabiulawal",
  "Rabiulakhir",
  "Jumadilawal",
  "Jumadilakhir",
  "Rajab",
  "Syakban",
  "Ramadan",
  "Syawal",
  "Zulkaidah",
  "Zulhijah",
];

$masehi_months = [
  "Januari",
  "Februari",
  "Maret",
  "April",
  "Mei",
  "Juni",
  "Juli",
  "Agustus",
  "September",
  "Oktober",
  "November",
  "Desember",
];

$schedules = execute_sql("
  SELECT
    schedules.id AS id,
    schedules.name AS name
  FROM schedules
  ORDER BY schedules.name ASC
")->fetchAll();

$schedule_changes = execute_sql("
  SELECT
    schedule_changes.id AS id,
    schedule_changes.date AS date,
    schedule_changes.month AS month,
    schedule_changes.calendar AS calendar,
    schedule_changes.schedule_id AS schedule_id,
    schedules.name AS schedule_name
  FROM schedule_changes
  JOIN schedules ON schedules.id = schedule_changes.schedule_id
  ORDER BY schedule_changes.month ASC, schedule_changes.date ASC
")->fetchAll();

// ----- VIEW -----

require_once __DIR__ . "/../components/basic_style.php";
require_once __DIR__ . "/../components/box.php";
require_once __DIR__ . "/../components/button.php";
require_once __DIR__ . "/../components/button_fab.php";
require_once __DIR__ . "/../components/event.php";
require_once __DIR__ . "/../components/field.php";
require_once __DIR__ . "/../components/head.php";
require_once __DIR__ . "/../components/icon.php";
require_once __DIR__ . "/../components/list_view.php";
require_once __DIR__ . "/../components/menu.php";
require_once __DIR__ . "/../components/modal.php";
require_once __DIR__ . "/../components/notification.php";
require_once __DIR__ . "/../components/page.php";
require_once __DIR__ . "/../components/sidebar.php";
require_once __DIR__ . "/../components/table.php";
require_once __DIR__ . "/../components/tag.php";

?><!DOCTYPE html>

<html lang="id">

<head>
  <title>Perubahan Paket Otomatis - Awqot</title>
  <?php publish("head"); ?>
  <style>
  .schedule_change-table td {
    height: 4rem;
    background-color: #fff;
  }
  </style>
</head>

<body>
  <?php publish("body"); ?>
  <div id="app" class="page" style="padding-top: 3.5rem;">
    <header class="page-header">
      <div class="page-header-row">
        <div class="page-header-left">
          <button type="button" class="button" onclick="ev.publish('sidebar:open');">
            <span class="icon">
              <?php include __DIR__ . "/../static/icons/round-menu-24px.svg" ?>
            </span>
          </button>
          <h1 class="page-title">Perubahan Paket Otomatis</h1>
        </div>
      </div>
    </header>

    <main class="page-content">
      <?php if (count($schedule_changes) === 0): ?>
      <p
        class="notification"
        onclick="ev.publish('modal:open', { dialog: schedule_change_create });"
      >Anda belum memiliki perubahan paket. Klik disini atau tombol tambah hijau di pojok bawah untuk membuat perubahan paket.</p>
      <?php else: ?>
      <section class="box">
        <div class="box-content">
          <table class="table schedule_change-table">
            <thead>
              <tr>
                <th>Tanggal</th>
                <th></th>
                <th>Paket</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($schedule_changes as $schedule_change): ?>
              <tr>
                <td><?= $schedule_change["date"] ?> <?= $schedule_change["calendar"] === "Hijriah" ? $hijriah_months[$schedule_change["month"] - 1] : $masehi_months[$schedule_change["month"] - 1] ?></td>
                <td>
                  <span class="icon" style="width: 2rem; height: 2rem;">
                    <?php include __DIR__ . "/../static/icons/round-arrow_right_alt-24px.svg" ?>
                  </span>
                </td>
                <td><?= $schedule_change["schedule_name"] ?></td>
                <td>
                  <form method="POST" style="display:flex; justify-content: flex-end;">
                    <input type="hidden" name="action" value="schedule_change_delete">
                    <input type="hidden" name="schedule_change_id" value="<?= $schedule_change["id"] ?>">
                    <button type="submit" class="button danger">
                      <span class="icon">
                        <?php include __DIR__ . "/../static/icons/outline-delete_forever-24px.svg" ?>
                      </span>
                    </button>
                  </form>
                </td>
              </tr>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
      </section>
      <?php endif ?>
    </main>

    <button type="button" class="button-fab" onclick="ev.publish('modal:open', { dialog: schedule_change_create });">
      <span class="icon">
        <?php include __DIR__ . "/../static/icons/round-add-24px.svg" ?>
      </span>
    </button>

    <!-- schedule_change_PACK -->
    <dialog id="schedule_change_create" class="modal hijriah">
      <form class="modal-shell" method="post">
        <header class="modal-header">
          <div class="modal-header-row">
            <div class="modal-header-left">
              <h4 class="modal-title">Buat Perubahan Paket</h4>
            </div>
          </div>
        </header>
        <div class="modal-content">
          <input type="hidden" name="action" value="schedule_change_create">
          <div class="field">
            <div class="field-label">
              <label for="calendar_input" class="label">Kalender</label>
            </div>
            <div class="field-input">
              <select id="calendar_input" name="calendar" oninput="ev.publish('calendar_input', event);">
                <option value="Hijriah" selected>Hijriah</option>
                <option value="Masehi">Masehi</option>
              </select>
            </div>
          </div>

          <div class="field">
            <div class="field-input">
              <label for="date_input" class="field-input-addons">Tanggal</label>
              <input
                id="date_input"
                class="input"
                type="number"
                name="date"
                style="max-width: 32px;"
                value="1"
                required
              >
              <label for="month_input" class="field-input-addons">Bulan</label>
              <select
                id="month_input"
                class="input"
                type="number"
                name="month"
                required
              >
                <option value="1" selected>Muharam</option>
                <option value="2">Safar</option>
                <option value="3">Rabiulawal</option>
                <option value="4">Rabiulakhir</option>
                <option value="5">Jumadilawal</option>
                <option value="6">Jumadilakhir</option>
                <option value="7">Rajab</option>
                <option value="8">Syakban</option>
                <option value="9">Ramadan</option>
                <option value="10">Syawal</option>
                <option value="11">Zulkaidah</option>
                <option value="12">Zulhijah</option>
              </select>
            </div>
          </div>

          <div class="field">
            <div class="field-label">
              <label for="">Ganti Ke Paket:</label>
            </div>
            <div class="field-input">
              <select id="schedule_input" name="schedule_id">
                <?php foreach ($schedules as $schedule): ?>
                <option value="<?= $schedule["id"] ?>"><?= $schedule["name"] ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
        </div>
        <footer class="modal-footer">
          <div class="modal-footer-left">
            <button
              type="button"
              class="button secondary"
              onclick="ev.publish('modal:close', { dialog: schedule_change_create });"
            >
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-close-24px.svg" ?>
              </span>
              <span class="text">Batal</span>
            </button>
          </div>
          <div class="modal-footer-right">
            <button type="submit" class="button primary">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-save-24px.svg" ?>
              </span>
              <span class="text">Simpan</span>
            </button>
          </div>
        </form>
      </footer>
    </dialog>
    <!-- /schedule_change_PACK -->
  </div>

  <script>
  var hijriahMonths = [
    'Muharam',
    'Safar',
    'Rabiulawal',
    'Rabiulakhir',
    'Jumadilawal',
    'Jumadilakhir',
    'Rajab',
    'Syakban',
    'Ramadan',
    'Syawal',
    'Zulkaidah',
    'Zulhijah',
  ];
  var masehiMonths = [
    'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember',
  ];
  ev.subscribe('calendar_input', function () {
    var calendarInput = document.getElementById('calendar_input');
    var monthInput = document.getElementById('month_input');
    if (calendarInput.value === 'Hijriah') {
      var months = hijriahMonths;
    } else if (calendarInput.value === 'Masehi') {
      var months = masehiMonths;
    }
    months.forEach(function (month, index) {
      monthInput.children[index].textContent = month;
    });
  });
  ev.publish('calendar_input');
  </script>
</body>

</html>
