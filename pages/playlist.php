<?php

require_once __DIR__ . "/../api/commands/awqot_ensure_upgrade.php";
require_once __DIR__ . "/../api/tools/database.php";
require_once __DIR__ . "/../api/tools/javascript.php";
require_once __DIR__ . "/../api/tools/time.php";

// ----- CONTEXT -----

$playlist_id = require_querystring("id");

// ----- CONTROLLER -----

if (isset($_POST["action"])) switch ($_POST["action"]) {
  case "playlist_name_set":
  execute_update_sql("playlists", [
    "name" => [$_POST["name"], PDO::PARAM_STR],
  ], [
    "id" => [$playlist_id, PDO::PARAM_INT],
  ]);
  break;

  case "materi_selector_input":
  $new_audios = array_filter($_POST["audio_hashes"], function ($audio) {
    return $audio !== "";
  });
  $saved_audios = execute_sql("
    SELECT audios
    FROM playlists
    WHERE id = :playlist_id
  ", [
    ":playlist_id" => [$playlist_id, PDO::PARAM_INT],
  ])->fetch();
  $saved_audios = json_decode($saved_audios["audios"], true) ?: [];
  foreach ($new_audios as $new_audio) {
    array_push($saved_audios, $new_audio);
  }
  execute_update_sql("playlists", [
    "audios" => [json_encode($saved_audios), PDO::PARAM_STR],
  ], [
    "id" => [$playlist_id, PDO::PARAM_INT],
  ]);
  break;

  case "playlist_audios_set":
  $audios = json_encode($_POST["audio_hashes"]);
  execute_update_sql("playlists", [
    "audios" => [$audios, PDO::PARAM_STR],
  ], [
    "id" => [$playlist_id, PDO::PARAM_INT],
  ]);
  break;

  case "playlist_delete":
  execute_delete_sql("playlists", [
    "id" => [$playlist_id, PDO::PARAM_INT],
  ]);
  header("Location: /pages/playlist-list.php");
  exit();
}

if ($_SERVER["REQUEST_METHOD"] === "POST") {
  header("Location: {$_SERVER["REQUEST_URI"]}");
  exit();
}

// ----- MODEL -----

$playlist = execute_sql("
  SELECT
    playlists.id AS id,
    playlists.name AS name,
    playlists.audios AS audios
  FROM playlists
  WHERE playlists.id = :playlist_id
", [
  ":playlist_id" => [$playlist_id, PDO::PARAM_INT],
])->fetch();

$playlist_audios = array_map(function ($audio_hash) {
  return execute_sql("
    SELECT
      audios.hash,
      audios.filename,
      audios.duration
    FROM audios
    WHERE audios.hash = :audio_hash
  ", [
    ":audio_hash" => [$audio_hash, PDO::PARAM_INT],
  ])->fetch();
}, json_decode($playlist["audios"], true));

$audios = execute_sql("
  SELECT
    audios.hash,
    audios.filename,
    audios.duration
  FROM audios
  ORDER BY audios.filename ASC
")->fetchAll();

// ----- VIEW -----

require_once __DIR__ . "/../components/basic_style.php";
require_once __DIR__ . "/../components/button.php";
require_once __DIR__ . "/../components/button_fab.php";
require_once __DIR__ . "/../components/event.php";
require_once __DIR__ . "/../components/field.php";
require_once __DIR__ . "/../components/head.php";
require_once __DIR__ . "/../components/icon.php";
require_once __DIR__ . "/../components/list_view.php";
require_once __DIR__ . "/../components/materi_selector.php";
require_once __DIR__ . "/../components/menu.php";
require_once __DIR__ . "/../components/modal.php";
require_once __DIR__ . "/../components/notification.php";
require_once __DIR__ . "/../components/page.php";
require_once __DIR__ . "/../components/tag.php";

?><!DOCTYPE html>

<html lang="id">

<head>
  <title>Playlist - Awqot</title>
  <?php publish("head"); ?>
  <style>
  .schedule-controls {
    background-color: #FFFFFF;
    position: fixed;
    border-radius: .4rem;
    right: 1.5rem;
    top: 40%;
    top: calc(50% - 4.5rem);
    transform: translateX(10rem);
    transition-duration: 300ms;
    transition-property: transform;
    box-shadow: 0 8px 17px 2px rgba(0,0,0,0.14),
      0 3px 14px 2px rgba(0,0,0,0.12),
      0 5px 5px -3px rgba(0,0,0,0.2);
  }
  .audio-selected .schedule-controls {
    transform: translateX(0);
  }
  .schedule-controls .button {
    min-height: 3.5rem;
    border-radius: .4rem;
  }
  .schedule-controls .button:focus,
  .schedule-controls .button:hover {
    background-color: rgba(0, 0, 0, .05);
  }
  .schedule-controls .button .icon {
    padding: 1.25rem 1rem;
    margin: 0 !important;
  }
  .schedule-controls .button:disabled {
    cursor: not-allowed; 
  }
  .schedule-controls .button:disabled:hover {
    cursor: not-allowed; 
    background-color: rgba(0, 0, 0, 0);
  }
  .schedule-controls .button:disabled .icon svg path:last-child {
    fill: #aaa;
  }
  </style>
</head>

<body>
  <?php publish("body"); ?>
  <div id="app" class="page" style="padding-top: 7rem;">
    <header class="page-header">
      <div class="page-header-row">
        <div class="page-header-left">
          <a class="button" href="/pages/playlist-list.php">
            <div class="icon">
              <?php include __DIR__ . "/../static/icons/round-chevron_left-24px.svg" ?>
            </div>
          </a>
          <h1 class="page-title">Playlist: <?= $playlist["name"] ?></h1>
        </div>
        <div class="page-header-right">
          <button
            type="button"
            class="button"
            onclick="ev.publish('modal:open', { dialog: document.getElementById('playlist') });"
          >
            <div class="icon">
              <?php include __DIR__ . "/../static/icons/round-edit-24px.svg" ?>
            </div>
          </button>
        </div>
      </div>
      <div class="page-header-row">
        <button type="button" class="button primary" onclick="ev.publish('playlist:play');">
          <span class="icon">
            <?php include __DIR__ . "/../static/icons/round-play_arrow-24px.svg" ?>
          </span>
        </button>
        <button type="button" class="button danger" onclick="ev.publish('playlist:stop_awqot')">
          <span class="icon">
            <?php include __DIR__ . "/../static/icons/round-stop-24px.svg" ?>
          </span>
        </button>
        <button type="button" class="button danger" onclick="ev.publish('playlist:speaker_on')">
          <span class="icon">
            <?php include __DIR__ . "/../static/icons/round-volume_up-24px.svg" ?>
          </span>
        </button>
        <button type="button" class="button danger" onclick="ev.publish('playlist:speaker_off')">
          <span class="icon">
            <?php include __DIR__ . "/../static/icons/round-volume_off-24px.svg" ?>
          </span>
        </button>
      </div>
    </header>

    <main class="page-content">
      <?php if (count($playlist_audios) === 0): ?>
      <p
        class="notification"
        onclick="ev.publish('materi_selector:open');"
      >Belum ada materi yang ditambahkan. Klik disini atau tombol tambah di pojok bawah untuk menambahkan materi.</p>
      <?php else: ?>
      <ul class="list-view selectable">
        <?php $total_duration = 0 ?>
        <?php foreach ($playlist_audios as $audio): $total_duration += $audio["duration"]; ?>
        <li
          class="list-view-item audio-item"
          onclick="ev.publish('playlist:audio_toggle_select', { item: this });"
          data-hash="<?= $audio["hash"] ?>"
          data-duration="<?= $audio["duration"] ?>"
        >
          <div class="list-view-item-row">
            <span
              class="duration"
              style="font-size: .9rem; color: #424242; padding-left: .75rem;"
            ><?= humanize_duration($audio["duration"]) ?></span>
            <span class="list-view-item-title">
              <span class="flex-ellipsis"><?= $audio["filename"] ?></span>
            </span>
          </div>
        </li>
        <?php endforeach ?>
      </ul>
      <?php if (count($playlist_audios) > 1): ?>
      <p
        style="
          box-sizing: border-box;
          max-width: 640px;
          margin: 0 auto;
          padding: .75rem .5rem;
          background-color: #EEE;
          color: #424242;
        "
      >
        <span>total durasi:</span>
        <strong
          style="display: inline-block; margin-left: .25rem; font-weight: 500;"
        ><?= humanize_duration($total_duration); ?></strong>
      </p>
      <?php endif ?>
      <form class="schedule-controls">
        <button type="button" class="button position-control" onclick="setAudioPosition('up')">
          <div class="icon">
            <?php include __DIR__ . "/../static/icons/round-arrow_upward-24px.svg" ?>
          </div>
        </button>
        <button type="button" class="button primary position-control" onclick="ev.publish('playlist:play');">
          <div class="icon">
            <?php include __DIR__ . "/../static/icons/round-play_arrow-24px.svg" ?>
          </div>
        </button>
        <button type="button" class="button danger" onclick="openAudiosDeleteModal();">
          <div class="icon">
            <?php include __DIR__ . "/../static/icons/round-delete_forever-24px.svg" ?>
          </div>
        </button>
        <button type="button" class="button position-control" onclick="setAudioPosition('down')">
          <div class="icon">
            <?php include __DIR__ . "/../static/icons/round-arrow_downward-24px.svg" ?>
          </div>
        </button>
      </form>
      <?php endif ?>
    </main>

    <button type="button" class="button-fab" onclick="ev.publish('materi_selector:open')">
      <span class="icon">
        <?php include __DIR__ . "/../static/icons/round-library_add-24px.svg" ?>
      </span>
      <span class="text">Materi Baru</span>
    </button>

    <!-- PLAYLIST -->
    <dialog id="playlist" class="modal">
      <div class="modal-overlay"></div>
      <form class="modal-shell" method="post">
        <header class="modal-header">
          <div class="modal-header-row">
            <div class="modal-header-left">
              <h4 class="modal-title">Atur Paket Audio</h4>
            </div>
            <div class="modal-header-right">
              <button
                type="button"
                class="button secondary"
                onclick="ev.publish('modal:close', { dialog: document.getElementById('playlist') });"
              >
                <span class="icon">
                  <?php include __DIR__ . "/../static/icons/round-close-24px.svg"; ?>
                </span>
              </button>
            </div>
          </div>
        </header>
        <div class="modal-content">
          <input type="hidden" name="action" value="playlist_name_set">
          <div class="field">
            <div class="field-label">
              <label class="label">Nama Paket</label>
            </div>
            <div class="field-input">
              <input
                ref="schedule_name_input"
                class="input"
                type="text"
                name="name"
                value="<?= $playlist["name"] ?>"
                placeholder="ketik disini..."
                required
              >
            </div>
          </div>
        </div>
        <footer class="modal-footer">
          <div class="modal-footer-left">
            <button
              type="button"
              class="button danger"
              onclick="ev.publish('modal:open', { dialog: playlist_delete });"
            >
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-delete_forever-24px.svg" ?>
              </span>
              <span class="text">Hapus Paket</span>
            </button>
          </div>
          <div class="modal-footer-right">
            <button type="submit" class="button primary">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-save-24px.svg" ?>
              </span>
              <span class="text">Perbarui</span>
            </button>
          </div>
        </form>
      </footer>
    </dialog>
    <!-- /PLAYLIST -->

    <!-- PLAYLIST_DELETE -->
    <dialog id="playlist_delete" class="modal">
      <form class="modal-shell" method="post">
        <div class="modal-content">
          <input type="hidden" name="action" value="playlist_delete">
          <input type="hidden" name="playlist_id">
          <p>Apakah anda yakin menghapus paket "<?= $playlist["name"] ?>"?</p>
        </div>
        <footer class="modal-footer">
          <div class="modal-footer-left">
            <button type="button" class="button secondary" onclick="ev.publish('modal:close', { dialog: playlist_delete });">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-close-24px.svg" ?>
              </span>
              <span class="text">Batal</span>
            </button>
          </div>
          <div class="modal-footer-right">
            <button type="submit" class="button danger">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-delete_forever-24px.svg" ?>
              </span>
              <span class="text">Hapus</span>
            </button>
          </div>
        </form>
      </footer>
    </dialog>
    <!-- /PLAYLIST_DELETE -->

    <!-- AUDIOS_DELETE -->
    <dialog id="audios_delete" class="modal">
      <div class="modal-overlay"></div>
      <form class="modal-shell" method="post">
        <div class="modal-content">
          <input type="hidden" name="action" value="playlist_audios_set">
          <p>Apakah anda yakin menghapus materi-materi yang terpilih?</p>
        </div>
        <footer class="modal-footer">
          <div class="modal-footer-left">
            <button type="button" class="button secondary" onclick="closeAudiosDeleteModal();">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-close-24px.svg" ?>
              </span>
              <span class="text">Batal</span>
            </button>
          </div>
          <div class="modal-footer-right">
            <button type="submit" class="button danger">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-delete_forever-24px.svg" ?>
              </span>
              <span class="text">Hapus</span>
            </button>
          </div>
        </form>
      </footer>
    </dialog>
    <!-- /AUDIOS_DELETE -->

    <!-- MATERI_PLAYER_PROMPT -->
    <dialog id="materi_player_prompt" class="modal">
      <div class="modal-overlay"></div>
      <form class="modal-shell" method="post">
        <div class="modal-content">
          <input type="hidden" name="action" value="audios_play">
          <p>Jalankan materi di perangkat?</p>
        </div>
        <footer class="modal-footer">
          <div class="modal-footer-left">
            <button type="button" class="button secondary" onclick="ev.publish('modal:close', { dialog: materi_player_prompt });">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-close-24px.svg" ?>
              </span>
              <span class="text">Batal</span>
            </button>
          </div>
          <div class="modal-footer-right">
            <button type="button" class="button secondary" onclick="ev.publish('playlist:play_smartphone');">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-smartphone-24px.svg" ?>
              </span>
              <span class="text">Smartphone</span>
            </button>
            <button type="button" class="button primary" onclick="ev.publish('playlist:play_awqot');">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-speaker-24px.svg" ?>
              </span>
              <span class="text">Awqot</span>
            </button>
          </div>
        </footer>
      </form>
    </dialog>
    <!-- /MATERI_PLAYER_PROMPT -->

    <dialog id="smartphone_player" class="modal">
      <div class="modal-overlay"></div>
      <div class="modal-shell">
        <div class="modal-content" style="display: flex;justify-content: center;padding: .5rem 0;">
          <audio src="" controls autoplay></audio>
        </div>
        <footer class="modal-footer">
          <div class="modal-footer-left"></div>
          <div class="modal-footer-right">
            <button type="button" class="button danger" onclick="ev.publish('playlist:stop_smartphone');">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-stop-24px.svg" ?>
              </span>
              <span class="text">Berhenti</span>
            </button>
          </div>
        </footer>
      </div>
    </dialog>
  </div>

  <?php export_to_js("playlist", $playlist); ?>

  <script>
function select(selector) {
  var elements = document.querySelectorAll(selector);
  return Array.prototype.slice.call(elements);
}

function openAudiosDeleteModal() {
  ev.publish('modal:open', { dialog: audios_delete });
  var deletedAudios = select('.audio-item.selected')
    .map(function (li) {
      return li.dataset.hash;
    });
  select('.audio-item')
    .map(function (li) {
      return li.dataset.hash;
    })
    .filter((audio_hash) => {
      return deletedAudios.indexOf(audio_hash) === -1;
    })
    .map(function (audio_hash) {
      var input = document.createElement('input');
      input.type = 'hidden';
      input.name = 'audio_hashes[]';
      input.value = audio_hash;
      return input;
    })
    .reduce(function (container, input) {
      container.appendChild(input);
      return container;
    }, document.querySelector('#audios_delete .modal-content'));
}

function closeAudiosDeleteModal() {
  ev.publish('modal:close', { dialog: audios_delete });
  var parent = document.querySelector('#audios_delete .modal-content');
  select('#audios_delete input[name="audio_hashes[]"]')
    .reduce(function (container, input) {
      container.removeChild(input);
      return container;
    }, document.querySelector('#audios_delete .modal-content'));
}

function setAudioPosition(direction) {
  select('.button.position-control').forEach(function (control) {
    control.disabled = true;
  });
  var request = new XMLHttpRequest();
  var data = new FormData();
  var audioInput = select('.schedule-controls [name="selected_audios[]"]')[0];
  var audioHash = audioInput.value;
  var audioIndex = parseInt(audioInput.dataset.index, 10);
  data.append('playlist_id', playlist.id);
  data.append('audio_hash', audioHash);
  data.append('audio_index', audioIndex);
  data.append('direction', direction);
  request.open('POST', '/api/playlist_audio_position_set.php', true);
  request.addEventListener('readystatechange', () => {
    if (request.readyState !== XMLHttpRequest.DONE) return;
    if (request.status !== 200) return alert(`MASALAH SERVER: ${request.responseText}`);
    var audioItemEl = document.querySelectorAll('.audio-item').item(audioIndex);
    if (direction === 'up' && audioItemEl.previousElementSibling) {
      audioItemEl.previousElementSibling.insertAdjacentElement('beforebegin', audioItemEl);
      audioInput.dataset.index = (audioIndex - 1).toString();
    } else if (direction === 'down' && audioItemEl.nextElementSibling) {
      audioItemEl.nextElementSibling.insertAdjacentElement('afterend', audioItemEl);
      audioInput.dataset.index = (audioIndex + 1).toString();
    }
    select('.button.position-control').forEach(function (control) {
      control.disabled = false;
    });
  });
  request.addEventListener('error', () => {
    alert(`MASALAH KONEKSI`);
  });
  request.send(data);
}

window.addEventListener('click', function (event) {
  if (event.target === document.children[0]) {
    select('.audio-item').forEach(function (audioItem) {
      ev.publish('playlist:audio_unselect', { item: audioItem });
    });
  }
});

ev.subscribe('playlist:audio_select', function audio_select(data) {
  if (data.item.classList.contains('selected')) return;
  data.item.classList.add('selected');
  var input = document.createElement('input');
  input.type = 'hidden';
  input.name = 'selected_audios[]';
  input.value = data.item.dataset.hash;
  input.dataset.index = select('.audio-item').indexOf(data.item);
  document.querySelector('.schedule-controls').appendChild(input);
  ev.publish('playlist:audio_toggle_select_change');
});

ev.subscribe('playlist:audio_unselect', function audio_unselect(data) {
  if (!data.item.classList.contains('selected')) return;
  data.item.classList.remove('selected');
  var input = document.querySelector('.schedule-controls [value="' + data.item.dataset.hash + '"]');
  input.parentNode.removeChild(input);
  ev.publish('playlist:audio_toggle_select_change');
});

ev.subscribe('playlist:audio_toggle_select', function audio_toggle_select(data) {
  if (data.item.classList.contains('selected')) {
    ev.publish('playlist:audio_unselect', data);
  } else {
    ev.publish('playlist:audio_select', data);
  }
});

ev.subscribe('playlist:audio_toggle_select_change', function audio_toggle_select_change() {
  var selectedLength = document.querySelectorAll('.audio-item.selected').length;
  if (selectedLength === 0) {
    document.body.classList.remove('audio-selected');
  } else {
    document.body.classList.add('audio-selected');
    select('.button.position-control').forEach(function (control) {
      control.disabled = selectedLength > 1;
    });
  }
});

ev.subscribe('playlist:play', function () {
  ev.publish('modal:open', { dialog: materi_player_prompt });
});

ev.subscribe('playlist:play_awqot', function () {
  var audioBegin = select('.audio-item.selected')[0];
  var audios = select('.audio-item');
  if (audioBegin) {
    audios = audios.slice(audios.indexOf(audioBegin));
  }
  audios = audios.map(function (audio) {
    return {
      audio_hash: audio.dataset.hash,
      duration: parseInt(audio.dataset.duration, 10),
    };
  });
  var audio_hashes = audios.map(function (audio) {
    return audio.audio_hash;
  }).join(',');
  var request = new XMLHttpRequest();
  request.open('POST', '/api/playlist_play.php?audio_hashes=' + audio_hashes);
  request.addEventListener('readystatechange', function () {
    if (request.readyState === request.DONE) {
      ev.publish('playlist:playing');
    }
  });
  request.send();
});

ev.subscribe('playlist:play_smartphone', function () {
  var audioBegin = select('.audio-item.selected')[0] || select('.audio-item')[0];
  if (!audioBegin) return;
  smartphone_player.querySelector('audio').src = '/data/audios/' + audioBegin.dataset.hash;
  ev.publish('modal:open', { dialog: smartphone_player });
  ev.publish('playlist:playing');
});

ev.subscribe('playlist:playing', function () {
  ev.publish('modal:close', { dialog: materi_player_prompt });
});

ev.subscribe('playlist:stop_awqot', function () {
  var request = new XMLHttpRequest();
  request.open('POST', '/api/playlist_stop.php');
  request.send();
});

ev.subscribe('playlist:stop_smartphone', function () {
  smartphone_player.querySelector('audio').src = '';
  ev.publish('modal:close', { dialog: smartphone_player });
});

ev.subscribe('playlist:speaker_on', function () {
  var request = new XMLHttpRequest();
  request.open('POST', '/api/commands/speaker_on.php');
  request.send();
});

ev.subscribe('playlist:speaker_off', function () {
  var request = new XMLHttpRequest();
  request.open('POST', '/api/commands/speaker_off.php');
  request.send();
});
  </script>
</body>

</html>
