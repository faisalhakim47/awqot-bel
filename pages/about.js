var awqotAdminData = JSON.parse(document.getElementById('awqot_admin_data').textContent);

function registerSubmit(form, event) {
  event.preventDefault();
  var submitButton = form.querySelector('button[type="submit"]');
  var submitText = submitButton.querySelector('.text');
  var originalSubmitText = submitText.textContent;
  submitButton.disabled = true;
  submitText.textContent = 'Menyimpan...';
  var data = new FormData(form)
  Promise.all([
    http.request('POST', 'https://admin.awqot.com/api/registerAwqotOwnership', { data: data }),
    http.request('POST', '/api/awqot_ownership_set.php', { data: data }),
  ]).then(function () {
    submitText.textContent = 'Tersimpan.';
    setTimeout(function () {
      submitButton.disabled = false;
      submitText.textContent = originalSubmitText;
      ev.publish('modal:close', { dialog: awqot_update_register_notice });
      ev.publish('modal:open', { dialog: awqot_update });
    }, 500);
  }).catch(function () {
    console.warn(error);
    alert('Gagal');
    submitButton.disabled = false;
    submitText.textContent = originalSubmitText;
  });
}

function saveModeChange() {
  http.request('POST', '/api/save_mode_change.php', {
    data: new FormData(save_mode_form),
  });
}
