<?php

require_once __DIR__ . "/../api/commands/awqot_ensure_upgrade.php";
require_once __DIR__ . "/../api/tools/database.php";

// ----- CONTROLLER -----

if (isset($_POST["action"])) switch ($_POST["action"]) {
  case "playlist_create":
  $playlist_id = execute_insert_sql("playlists", [
    "name" => [$_POST["name"], PDO::PARAM_STR],
    "audios" => ["[]", PDO::PARAM_STR],
  ]);
  header("Location: /pages/playlist.php?id={$playlist_id}");
  exit();
}

// ----- MODEL -----

$playlists = execute_sql("
  SELECT
    playlists.id AS id,
    playlists.name AS name
  FROM playlists
  ORDER BY playlists.name ASC
")->fetchAll();

// ----- VIEW -----

require_once __DIR__ . "/../components/basic_style.php";
require_once __DIR__ . "/../components/button.php";
require_once __DIR__ . "/../components/button_fab.php";
require_once __DIR__ . "/../components/event.php";
require_once __DIR__ . "/../components/field.php";
require_once __DIR__ . "/../components/head.php";
require_once __DIR__ . "/../components/icon.php";
require_once __DIR__ . "/../components/list_view.php";
require_once __DIR__ . "/../components/menu.php";
require_once __DIR__ . "/../components/modal.php";
require_once __DIR__ . "/../components/notification.php";
require_once __DIR__ . "/../components/page.php";
require_once __DIR__ . "/../components/sidebar.php";
require_once __DIR__ . "/../components/tag.php";

?><!DOCTYPE html>

<html lang="id">

<head>
  <title>Materi Player - Awqot</title>
  <?php publish("head"); ?>
</head>

<body>
  <?php publish("body"); ?>
  <div id="app" class="page" style="padding-top: 3.5rem;">
    <header class="page-header">
      <div class="page-header-row">
        <div class="page-header-left">
          <button type="button" class="button" onclick="ev.publish('sidebar:open');">
            <div class="icon">
              <?php include __DIR__ . "/../static/icons/round-menu-24px.svg" ?>
            </div>
          </button>
          <h1 class="page-title">Playlist</h1>
        </div>
      </div>
    </header>

    <main class="page-content">
      <?php if (count($playlists) === 0): ?>
      <p
        class="notification"
        onclick="ev.publish('modal:open', { dialog: playlist_create });"
      >Anda belum memiliki playlist. Klik disini atau tombol tambah hijau di pojok bawah untuk membuat playlist.</p>
      <?php endif ?>
      <ul class="list-view">
        <?php foreach ($playlists as $playlist): ?>
        <li class="list-view-item" style="cursor: pointer;">
          <div class="list-view-item-row">
            <a
              class="list-view-item-title"
              href="/pages/playlist.php?id=<?= $playlist["id"] ?>"
            ><?= $playlist["name"] ?></a>
          </div>
        </li>
        <?php endforeach ?>
      </ul>
    </main>

    <button type="button" class="button-fab" onclick="ev.publish('modal:open', { dialog: playlist_create });">
      <span class="icon">
        <?php include __DIR__ . "/../static/icons/round-add-24px.svg" ?>
      </span>
      <span class="text">Playlist Baru</span>
    </button>

    <!-- PLAYLIST_PACK -->
    <dialog id="playlist_create" class="modal">
      <form class="modal-shell" method="post">
        <header class="modal-header">
          <div class="modal-header-row">
            <div class="modal-header-left">
              <h4 class="modal-title">Buat Playlist</h4>
            </div>
          </div>
        </header>
        <div class="modal-content">
          <input type="hidden" name="action" value="playlist_create">
          <div class="field">
            <div class="field-label">
              <label class="label">Nama Playlist</label>
            </div>
            <div class="field-input">
              <input
                ref="playlist_name_input"
                class="input"
                type="text"
                name="name"
                placeholder="ketik disini..."
                required
              >
            </div>
          </div>
        </div>
        <footer class="modal-footer">
          <div class="modal-footer-left">
            <button
              type="button"
              class="button secondary"
              onclick="ev.publish('modal:close', { dialog: playlist_create });"
            >
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-close-24px.svg" ?>
              </span>
              <span class="text">Batal</span>
            </button>
          </div>
          <div class="modal-footer-right">
            <button type="submit" class="button primary">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-save-24px.svg" ?>
              </span>
              <span class="text">Simpan</span>
            </button>
          </div>
        </form>
      </footer>
    </dialog>
    <!-- /PLAYLIST_PACK -->
  </div>
</body>

</html>
