<?php

require_once __DIR__ . "/../api/commands/awqot_ensure_upgrade.php";
require_once __DIR__ . "/../api/tools/database.php";
require_once __DIR__ . "/../api/tools/javascript.php";

// ----- CONTROLLER -----

if (isset($_POST["action"]) && $_POST["action"] === "create_schedule") {
  $schedule_id = execute_insert_sql("schedules", [
    "name" => [$_POST["name"], PDO::PARAM_STR],
  ]);
  header("Location: /pages/schedule.php?schedule_id={$schedule_id}");
  exit();
}

else if (isset($_POST["action"]) && $_POST["action"] === "delete_schedule") {
  $schedule_id = $_POST["schedule_id"];
  execute_delete_sql("schedule_times", [
    "schedule_id" => [$schedule_id, PDO::PARAM_INT],
  ]);
  execute_delete_sql("schedules", [
    "id" => [$schedule_id, PDO::PARAM_INT],
  ]);
  header("Location: /pages/schedule-list.php");
}

else if (isset($_POST["action"]) && $_POST["action"] === "activate_schedule") {
  set_configuration("active_schedule_id", $_POST["schedule_id"]);
}

else if (isset($_POST["action"]) && $_POST["action"] === "schedule_duplicate") {
  $schedule_times = execute_sql("
    SELECT
      schedule_times.day AS day,
      schedule_times.hour AS hour,
      schedule_times.minute AS minute,
      schedule_times.audios AS audios
    FROM schedule_times
    WHERE schedule_times.schedule_id = :schedule_id
  ", [
    ":schedule_id" => [$_POST["schedule_id"], PDO::PARAM_INT],
  ])->fetchALl();
  $schedule_id = execute_insert_sql("schedules", [
    "name" => [$_POST["name"], PDO::PARAM_STR],
  ]);
  foreach ($schedule_times as $schedule_time) {
    execute_insert_sql("schedule_times", [
      "schedule_id" => [$schedule_id, PDO::PARAM_INT],
      "day" => [$schedule_time["day"], PDO::PARAM_INT],
      "hour" => [$schedule_time["hour"], PDO::PARAM_INT],
      "minute" => [$schedule_time["minute"], PDO::PARAM_INT],
      "audios" => [$schedule_time["audios"], PDO::PARAM_STR],
    ]);
  }
}

if ($_SERVER["REQUEST_METHOD"] === "POST") {
  require_once __DIR__ . "/../api/commands/scheduling_compute.php";
  header("Location: {$_SERVER["REQUEST_URI"]}");
  exit();
}

// ----- MODEL -----

$schedules = execute_sql("
  SELECT
    schedules.id AS id,
    schedules.name AS name
  FROM schedules
  ORDER BY schedules.name ASC
")->fetchAll();

$schedule_config = execute_sql("
  SELECT
    configurations.name AS name,
    configurations.value AS value
  FROM configurations
  WHERE configurations.name = 'active_schedule_id'
")->fetchAll();

require_once __DIR__ . "/../components/sidebar.php";

?><!DOCTYPE html>

<html lang="id">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Paket Jadwal – Awqot Bel</title>
  <?php publish("head"); ?>
  <?php
  echo "<style>";
  require_once __DIR__ . "/../static/styles/normalize.css";
  require_once __DIR__ . "/../static/styles/default.css";
  require_once __DIR__ . "/../static/styles/tools.css";
  require_once __DIR__ . "/../static/styles/transitions.css";
  require_once __DIR__ . "/../static/styles/components/page.css";
  require_once __DIR__ . "/../static/styles/components/icon.css";
  require_once __DIR__ . "/../static/styles/components/button.css";
  require_once __DIR__ . "/../static/styles/components/fab.css";
  require_once __DIR__ . "/../static/styles/components/modal.css";
  require_once __DIR__ . "/../static/styles/components/input.css";
  require_once __DIR__ . "/../static/styles/components/list-view.css";
  require_once __DIR__ . "/../static/styles/components/notification.css";
  require_once __DIR__ . "/../static/styles/components/field.css";
  require_once __DIR__ . "/../static/styles/components/menu.css";
  require_once __DIR__ . "/../static/styles/components/tag.css";
  echo "</style>";
  ?>
</head>

<body>
  <?php publish("body"); ?>
  <div id="app" class="page" style="padding-top: 3.5rem;">
    <transition name="fade">
    <section
      v-if="!isLoaded"
      style="
        position: fixed;
        top: 0;
        left: 0;
        height: 100vh;
        width: 100vw;
        background-color: #FFFFFF;
        z-index: 1000;
      "
    ></section>
    </transition>
    <header class="page-header">
      <div class="page-header-row">
        <div class="page-header-left">
          <button type="button" class="button" onclick="ev.publish('sidebar:open');">
            <div class="icon">
              <?php include __DIR__ . "/../static/icons/round-menu-24px.svg" ?>
            </div>
          </button>
          <h1 class="page-title">Paket Jadwal</h1>
        </div>
      </div>
    </header>

    <main class="page-content">
      <p
        v-if="scheduleList.length === 0"
        class="notification"
        @click="modalCreateSchedule = true"
      >Anda belum memiliki paket jadwal. Silahkan buat paket jadwal terlebih dahulu.</p>
      <ul class="list-view selectable">
        <li
          v-for="schedule in scheduleList"
          :key="schedule.id"
          class="list-view-item"
        >
          <div class="list-view-item-row">
            <a
              class="list-view-item-title"
              :href="'/pages/schedule.php?schedule_id=' + schedule.id"
            >{{ schedule.name }}</a>
            <span v-if="activeScheduleId === schedule.id" class="tag">aktif</span>
            <form v-else method="post">
              <input type="hidden" name="action" value="activate_schedule">
              <input type="hidden" name="schedule_id" :value="schedule.id">
              <button
                type="submit"
                class="button"
                :disabled="activeScheduleId === schedule.id"
                style="width: 100%;"
              >
                <span class="text">Aktifkan</span>
              </button>
            </form>
            <button
              type="button"
              class="button"
              data-menu
              v-click-outside="(event) => {
                let node = event.target;
                while (node) {
                  if (node.dataset && 'menu' in node.dataset) return;
                  node = node.parentNode;
                }
                activeMenu = false;
              }"
              @click="activeMenu = activeMenu !== schedule.id ? schedule.id : false "
            >
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-more_vert-24px.svg" ?>
              </span>
            </button>
            <transition name="fade">
            <div
              v-if="activeMenu === schedule.id"
              class="menu activated"
            >
              <ul class="menu-list">
                <li class="menu-item">
                  <a class="button" :href="'/pages/schedule.php?schedule_id=' + schedule.id">
                    <span class="icon">
                      <?php include __DIR__ . "/../static/icons/round-edit-24px.svg" ?>
                    </span>
                    <span class="text">Edit</span>
                  </a>
                </li>
                <li class="menu-item">
                  <button type="button" class="button" @click="scheduleDuplicateId = schedule.id">
                    <span class="icon">
                      <?php include __DIR__ . "/../static/icons/round-file_copy-24px.svg" ?>
                    </span>
                    <span class="text">Duplikat</span>
                  </button>
                </li>
                <li class="menu-item">
                  <button type="button" class="button" @click="scheduleDeleteId = schedule.id">
                    <span class="icon">
                      <?php include __DIR__ . "/../static/icons/outline-delete_forever-24px.svg" ?>
                    </span>
                    <span class="text">Hapus</span>
                  </button>
                </li>
              </ul>
            </div>
            </transition>
          </div>
        </li>
      </ul>
    </main>

    <!-- <aside id="sidebar" class="sidebar">
      <header class="sidebar-header">

      </header>
      <nav class="sidebar-nav">
        <ul class="sidebar-nav-group-list">
          <li class="sidebar-nav-group-item">
            <span class="sidebar-nav-group-title">Home</span>
            <ul class="sidebar-nav-list">
              <li class="sidebar-nav-item">
                <a href="#">

                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
    </aside> -->

    <button type="button" class="fab" @click="modalCreateSchedule = true">
      <span class="icon">
        <?php include __DIR__ . "/../static/icons/round-add-24px.svg" ?>
      </span>
    </button>

    <!-- SCHEDULE_PACK -->
    <transition name="fade">
    <dialog v-if="modalCreateSchedule" class="modal" open>
      <div class="modal-overlay"></div>
      <form class="modal-shell" method="post">
        <header class="modal-header">
          <div class="modal-header-row">
            <div class="modal-header-left">
              <h4 class="modal-title">Buat Paket Jadwal</h4>
            </div>
          </div>
        </header>
        <div class="modal-content">
          <input type="hidden" name="action" value="create_schedule">
          <div class="field">
            <div class="field-label">
              <label class="label">Nama Paket</label>
            </div>
            <div class="field-input">
              <input
                ref="schedule_name_input"
                class="input"
                type="text"
                name="name"
                placeholder="ketik disini..."
                required
              >
            </div>
          </div>
        </div>
        <footer class="modal-footer">
          <div class="modal-footer-left">
            <button type="button" class="button secondary" @click="modalCreateSchedule = false">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-close-24px.svg" ?>
              </span>
              <span class="text">Batal</span>
            </button>
          </div>
          <div class="modal-footer-right">
            <button type="submit" class="button primary">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-save-24px.svg" ?>
              </span>
              <span class="text">Simpan</span>
            </button>
          </div>
        </form>
      </footer>
    </dialog>
    </transition>
    <!-- /SCHEDULE_PACK -->

    <!-- SCHEDULE_PACK_DELETE -->
    <transition name="fade">
    <dialog v-if="scheduleDeleteId !== 0" class="modal" open>
      <div class="modal-overlay"></div>
      <form class="modal-shell" method="post">
        <div class="modal-content">
          <input type="hidden" name="action" value="delete_schedule">
          <input type="hidden" name="schedule_id" :value="scheduleDeleteId">
          <p>Apakah anda yakin menghapus paket ini?</p>
        </div>
        <footer class="modal-footer">
          <div class="modal-footer-left">
            <button type="button" class="button secondary" @click="scheduleDeleteId = 0">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-close-24px.svg" ?>
              </span>
              <span class="text">Batal</span>
            </button>
          </div>
          <div class="modal-footer-right">
            <button type="submit" class="button danger">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-delete_forever-24px.svg" ?>
              </span>
              <span class="text">Hapus</span>
            </button>
          </div>
        </form>
      </footer>
    </dialog>
    </transition>
    <!-- /SCHEDULE_PACK_DELETE -->

    <!-- SCHEDULE_PACK_DUPLICATE -->
    <transition name="fade">
    <dialog v-if="scheduleDuplicateId !== 0" class="modal" open>
      <div class="modal-overlay"></div>
      <form class="modal-shell" method="post">
        <header class="modal-header">
          <div class="modal-header-row">
            <div class="modal-header-left">
              <h4 class="modal-title">Duplikat Paket "{{ activeScheduleDuplicate.name }}"</h4>
            </div>
          </div>
        </header>
        <div class="modal-content">
          <input type="hidden" name="action" value="schedule_duplicate">
          <input type="hidden" name="schedule_id" :value="scheduleDuplicateId">
          <div class="field">
            <div class="field-label">
              <label class="label">Nama Paket Baru</label>
            </div>
            <div class="field-input">
              <input
                class="input"
                type="text"
                name="name"
                placeholder="ketik disini..."
                required
              >
            </div>
          </div>
        </div>
        <footer class="modal-footer">
          <div class="modal-footer-left">
            <button type="button" class="button secondary" @click="scheduleDuplicateId = 0">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-close-24px.svg" ?>
              </span>
              <span class="text">Batal</span>
            </button>
          </div>
          <div class="modal-footer-right">
            <button type="submit" class="button primary">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-save-24px.svg" ?>
              </span>
              <span class="text">Duplikat</span>
            </button>
          </div>
        </form>
      </footer>
    </dialog>
    </transition>
    <!-- /SCHEDULE_PACK_DUPLICATE -->

  </div>

  <?php export_to_js("schedule_config", $schedule_config); ?>

  <script src="../static/scripts/vue.min.js"></script>
  <script src="../static/scripts/v-click-outside.min.min.umd.js"></script>
  <script>
    Vue.use(window['v-click-outside']);

    new Vue({
      el: document.getElementById('app'),
      data() {
        let active_schedule_config = window.schedule_config.find((config) => {
          return config.name === 'active_schedule_id';
        });
        active_schedule_config = active_schedule_config ? active_schedule_config.value : 0;
        return {
          isLoaded: false,
          isSaving: false,
          activeMenu: false,
          modalCreateSchedule: false,
          scheduleDeleteId: 0,
          scheduleDuplicateId: 0,
          activeScheduleId: parseInt(active_schedule_config, 10),
          scheduleList: <?= json_encode($schedules) ?>,
        };
      },

      computed: {
        activeScheduleDuplicate() {
          return this.scheduleList.find((schedule) => {
            return schedule.id === this.scheduleDuplicateId;
          });
        },
      },

      methods: {
        openSchedule(schedule_id) {
          window.location = '/pages/schedule.php?schedule_id=' + schedule_id;
        },
      },

      watch: {
        modalCreateSchedule(modalCreateSchedule) {
          if (modalCreateSchedule) {
            setTimeout(() => {
              this.$refs.schedule_name_input.focus();
            });
          }
        },
      },

      mounted() {
        this.$nextTick().then(() => this.isLoaded = true);
        const unload = () => this.isLoaded = false;
        window.addEventListener('beforeunload', unload);
        window.addEventListener('unload', unload);
      },
    });
  </script>
</body>

</html>
