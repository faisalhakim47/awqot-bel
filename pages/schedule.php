<?php

require_once __DIR__ . "/../api/commands/awqot_ensure_upgrade.php";
require_once __DIR__ . "/../api/tools/configuration.php";
require_once __DIR__ . "/../api/tools/database.php";
require_once __DIR__ . "/../api/tools/javascript.php";

// ----- CONTEXT -----

$schedule_id = (int) require_querystring("schedule_id");

$audios = isset($_POST["audios"])
  ? $_POST["audios"]
  : [];

// ----- CONTROLLER -----

if (isset($_POST["action"]) && $_POST["action"] === "update_schedule") {
  execute_update_sql("schedules", [
    "name" => [$_POST["name"], PDO::PARAM_STR],
  ], [
    "id" => [$schedule_id, PDO::PARAM_INT],
  ]);
}

else if (isset($_POST["action"]) && $_POST["action"] === "delete_schedule") {
  execute_delete_sql("schedule_times", [
    "schedule_id" => [$schedule_id, PDO::PARAM_INT],
  ]);
  execute_delete_sql("schedules", [
    "id" => [$schedule_id, PDO::PARAM_INT],
  ]);
  header("Location: /pages/schedule-list.php");
}

else if (isset($_POST["action"]) && $_POST["action"] === "create_schedule_time") {
  $relay_pins = array_filter(array_map(function ($pin) {
    return (int) $pin;
  }, $_POST["relay_pins"]), function ($pin) {
    return !!$pin;
  });
  sort($relay_pins);
  execute_insert_sql("schedule_times", [
    "schedule_id" => [$schedule_id, PDO::PARAM_INT],
    "day" => [$_POST["day"], PDO::PARAM_INT],
    "hour" => [$_POST["hour"], PDO::PARAM_INT],
    "minute" => [$_POST["minute"], PDO::PARAM_INT],
    "audios" => [json_encode($audios), PDO::PARAM_STR],
    "relay_pins" => [json_encode($relay_pins), PDO::PARAM_STR],
  ]);
}

else if (isset($_POST["action"]) && $_POST["action"] === "update_schedule_time") {
  $relay_pins = array_filter(array_map(function ($pin) {
    return (int) $pin;
  }, $_POST["relay_pins"]), function ($pin) {
    return !!$pin;
  });
  sort($relay_pins);
  execute_update_sql("schedule_times", [
    "schedule_id" => [$schedule_id, PDO::PARAM_INT],
    "day" => [$_POST["day"], PDO::PARAM_INT],
    "hour" => [$_POST["hour"], PDO::PARAM_INT],
    "minute" => [$_POST["minute"], PDO::PARAM_INT],
    "audios" => [json_encode($audios), PDO::PARAM_STR],
    "relay_pins" => [json_encode($relay_pins), PDO::PARAM_STR],
  ], [
    "id" => [$_POST["id"], PDO::PARAM_INT],
  ]);
}

else if (isset($_POST["action"]) && $_POST["action"] === "delete_schedule_time") {
  execute_delete_sql("schedule_times", [
    "id" => [$_POST["id"], PDO::PARAM_INT],
  ]);
}

else if (isset($_POST["action"]) && $_POST["action"] === "duplicate_days") {
  $schedule_times = execute_sql("
    SELECT
      schedule_times.hour AS hour,
      schedule_times.minute AS minute,
      schedule_times.audios AS audios,
      schedule_times.relay_pins AS relay_pins
    FROM schedule_times
    WHERE schedule_times.day = :day
      AND schedule_times.schedule_id = :schedule_id
  ", [
    ":schedule_id" => [$schedule_id, PDO::PARAM_INT],
    ":day" => [$_POST["source_day_id"], PDO::PARAM_INT],
  ])->fetchAll();
  foreach ($_POST["target_day_ids"] as $day_id) {
    execute_delete_sql("schedule_times", [
      "schedule_id" => [$schedule_id, PDO::PARAM_INT],
      "day" => [$day_id, PDO::PARAM_INT],
    ]);
    foreach ($schedule_times as $schedule_time) {
      execute_insert_sql("schedule_times", [
        "schedule_id" => [$schedule_id, PDO::PARAM_INT],
        "day" => [$day_id, PDO::PARAM_INT],
        "hour" => [$schedule_time["hour"], PDO::PARAM_INT],
        "minute" => [$schedule_time["minute"], PDO::PARAM_INT],
        "audios" => [$schedule_time["audios"], PDO::PARAM_INT],
        "relay_pins" => [$schedule_time["relay_pins"], PDO::PARAM_STR],
      ]);
    }
  }
}

if ($_SERVER["REQUEST_METHOD"] === "POST") {
  require_once __DIR__ . "/../api/commands/scheduling_compute.php";
  header("Location: {$_SERVER["REQUEST_URI"]}");
}

// ----- MODEL -----

$schedule = execute_sql("
  SELECT
    schedules.id AS id,
    schedules.name AS name
  FROM schedules
  WHERE schedules.id = :schedule_id
", [
  ":schedule_id" => [$schedule_id, PDO::PARAM_INT],
])->fetch();

if ($schedule === false) {
  header("Location: /pages/schedule-list.php");
}

$schedule_times = execute_sql("
  SELECT
    schedule_times.id AS id,
    schedule_times.day AS day,
    schedule_times.hour AS hour,
    schedule_times.minute AS minute,
    schedule_times.audios AS audios,
    schedule_times.relay_pins AS relay_pins
  FROM schedule_times
  WHERE schedule_times.schedule_id = :schedule_id
  ORDER BY day ASC, hour ASC, minute ASC
", [
  ":schedule_id" => [$schedule_id, PDO::PARAM_INT],
])->fetchAll();

$audios = execute_sql("
  SELECT
    audios.hash,
    audios.filename,
    audios.duration
  FROM audios
  ORDER BY audios.filename ASC
")->fetchAll();

$speaker_pins = json_decode(get_configuration("speaker_pins"), false);

// ----- VIEW -----

require_once __DIR__ . "/../components/basic_style.php";
require_once __DIR__ . "/../components/button.php";
require_once __DIR__ . "/../components/button_fab.php";
require_once __DIR__ . "/../components/event.php";
require_once __DIR__ . "/../components/field.php";
require_once __DIR__ . "/../components/field.php";
require_once __DIR__ . "/../components/head.php";
require_once __DIR__ . "/../components/icon.php";
require_once __DIR__ . "/../components/list_view.php";
require_once __DIR__ . "/../components/materi_player.php";
require_once __DIR__ . "/../components/menu.php";
require_once __DIR__ . "/../components/modal.php";
require_once __DIR__ . "/../components/notification.php";
require_once __DIR__ . "/../components/page.php";
require_once __DIR__ . "/../components/sidebar.php";
require_once __DIR__ . "/../components/speaker.php";
require_once __DIR__ . "/../components/tag.php";

?><!DOCTYPE html>

<html lang="id">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title><?= $schedule["name"] ?> – Awqot Bel</title>
  <?php publish("head"); ?>
  <style>
    #materi_player_prompt {
      z-index: 10;
    }
    #materi_player_speaker_prompt {
      z-index: 20;
    }
    #materi_player {
      z-index: 30;
    }
  </style>
</head>

<body>
  <?php publish("body"); ?>
  <div id="app" class="page" style="padding-top: 6.5rem;">
    <transition name="fade">
    <section
      v-if="!isLoaded"
      style="
        position: fixed;
        top: 0;
        left: 0;
        height: 100vh;
        width: 100vw;
        background-color: #FFFFFF;
        z-index: 1000;
      "
    ></section>
    </transition>
    <header class="page-header">
      <div class="page-header-row">
        <div class="page-header-left">
          <a class="button" href="/pages/schedule-list.php">
            <span class="icon">
              <?php include __DIR__ . "/../static/icons/round-chevron_left-24px.svg"; ?>
            </span>
          </a>
          <h1 class="page-title"><?= $schedule["name"] ?></h1>
        </div>
        <div class="page-header-right">
          <button type="button" class="button" @click="modalSchedule = true">
            <span class="icon">
              <?php include __DIR__ . "/../static/icons/round-settings-24px.svg"; ?>
            </span>
          </button>
        </div>
      </div>
      <nav class="page-header-row">
        <ul class="page-header-tab primary">
          <li
            v-for="(day, index) in days"
            :key="day.id"
            class="page-header-tab-item"
            :class="{
              'activated': day === activeDay,
              'is-preceding-active': (days.indexOf(activeDay) - 1) === index,
            }"
          >
            <a :href="'#' + day.value">{{ day.label }}</a>
          </li>
        </ul>
      </nav>
    </header>

    <main class="page-content" style="padding-bottom: 160px;">
      <p
        v-if="activeDay.times.length === 0"
        class="notification"
      >jadwal belum ditambahkan pada hari {{ activeDay.label }}.</p>
      <ul class="list-view selectable">
        <li
          v-for="schedule_time in activeDay.times"
          :key="schedule_time.id"
          class="list-view-item"
          style="padding: .75rem 0 .75rem;"
          @click="openScheduleTime(schedule_time)"
        >
          <div class="list-view-item-row">
            <span
              class="list-view-item-title"
              style="font-size: 1.25rem;"
            >{{ toDigit(schedule_time.hour, 2) }}:{{ toDigit(schedule_time.minute, 2) }} <small style="font-size: .82rem; margin-left: 4px;">({{ relayPinDisplay(schedule_time.relayPins) }})</small></span>
            <span
              class="list-view-item-subtitle"
              style="
                font-size: .96rem;
              "
            ><span style="font-size: .96em; color: #616161;">total durasi </span> {{ durationFormat(schedule_time.audios.map(a => a.duration).reduce((a, b) => a + b, 0)) }}</span>
          </div>
          <div class="list-view-item-row">
            <p
              v-if="schedule_time.audios.length === 0"
              class="notification"
            >Materi audio belum ditambahkan.</p>
            <ul
              v-else
              style="
                user-select: none;
                -moz-user-select: none;
                box-sizing: border-box;
                list-style: none;
                margin: 0;
                padding: 0 .75rem;
                max-width: 100%;
                font-size: .96rem;
                flex: 1;
              "
            >
              <li
                v-for="(audio, index) in schedule_time.audios"
                :key="index + audio.hash"
                class="level"
                style="display: flex; margin: .25rem 0;"
              >
                <div class="flex-ellipsis" style="color: #424242;">{{ audio.filename }}</div>
                <div
                  style="box-sizing: border-box; padding-left: .5rem; color: #616161;"
                >{{ durationFormat(audio.duration) }}</div>
              </li>
            </ul>
          </div>
        </li>
      </ul>
    </main>

    <button type="button" class="fab" @click="modalScheduleTime = true">
      <span class="icon">
        <?php include __DIR__ . "/../static/icons/round-add-24px.svg" ?>
      </span>
    </button>

    <button type="button" class="fab secondary" @click="modalDuplicate = true">
      <span class="icon">
        <?php include __DIR__ . "/../static/icons/round-file_copy-24px.svg" ?>
      </span>
    </button>

    <!-- SCHEDULE_PACK -->
    <transition name="fade">
    <dialog v-if="modalSchedule" class="modal" open>
      <div class="modal-overlay"></div>
      <form class="modal-shell" method="post">
        <header class="modal-header">
          <div class="modal-header-row">
            <div class="modal-header-left">
              <h4 class="modal-title">Atur Paket Jadwal</h4>
            </div>
            <div class="modal-header-right">
              <button type="button" class="button secondary" @click="modalSchedule = false">
                <span class="icon">
                  <?php include __DIR__ . "/../static/icons/round-close-24px.svg"; ?>
                </span>
              </button>
            </div>
          </div>
        </header>
        <div class="modal-content">
          <input type="hidden" name="action" value="update_schedule">
          <div class="field">
            <div class="field-label">
              <label class="label">Nama Paket</label>
            </div>
            <div class="field-input">
              <input
                ref="schedule_name_input"
                class="input"
                type="text"
                name="name"
                value="<?= $schedule["name"] ?>"
                placeholder="ketik disini..."
                required
              >
            </div>
          </div>
        </div>
        <footer class="modal-footer">
          <div class="modal-footer-left">
            <button type="button" class="button danger" @click="modalScheduleDelete = true">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-delete_forever-24px.svg" ?>
              </span>
              <span class="text">Hapus Paket</span>
            </button>
          </div>
          <div class="modal-footer-right">
            <button type="submit" class="button primary">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-save-24px.svg" ?>
              </span>
              <span class="text">Perbarui</span>
            </button>
          </div>
        </form>
      </footer>
    </dialog>
    </transition>
    <!-- /SCHEDULE_PACK -->

    <!-- SCHEDULE_PACK_DELETE -->
    <transition name="fade">
    <dialog v-if="modalScheduleDelete" class="modal" open>
      <div class="modal-overlay"></div>
      <form class="modal-shell" method="post">
        <div class="modal-content">
          <input type="hidden" name="action" value="delete_schedule">
          <p>Apakah anda yakin menghapus paket ini?</p>
        </div>
        <footer class="modal-footer">
          <div class="modal-footer-left">
            <button type="button" class="button secondary" @click="modalScheduleDelete = false">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-close-24px.svg" ?>
              </span>
              <span class="text">Batal</span>
            </button>
          </div>
          <div class="modal-footer-right">
            <button type="submit" class="button danger">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-delete_forever-24px.svg" ?>
              </span>
              <span class="text">Hapus</span>
            </button>
          </div>
        </form>
      </footer>
    </dialog>
    </transition>
    <!-- /SCHEDULE_PACK_DELETE -->

    <!-- SCHEDULE_TIME -->
    <transition name="fade">
    <dialog v-if="modalScheduleTime" class="modal" open>
      <div class="modal-overlay"></div>
      <form class="modal-shell" method="post" enctype="multipart/form-data">
        <header class="modal-header">
          <div class="modal-header-row">
            <div class="modal-header-left">
              <h4 class="modal-title">{{ isNewTime ? 'Tambah Jadwal' : 'Edit Jadwal' }}</h4>
            </div>
            <div class="modal-header-right">
              <button type="button" class="button secondary" @click="modalScheduleTime = false">
                <span class="icon">
                  <?php include __DIR__ . "/../static/icons/round-close-24px.svg" ?>
                </span>
              </button>
            </div>
          </div>
        </header>
        <div class="modal-content">
          <input v-if="isNewTime" type="hidden" name="action" value="create_schedule_time">
          <input v-if="!isNewTime" type="hidden" name="action" value="update_schedule_time">
          <input v-if="!isNewTime" type="hidden" name="id" :value="scheduleTime.id">
          <input type="hidden" name="day" :value="activeDay.id">
          <div class="field">
            <div class="field-input">
              <span class="field-input-addons">Pukul</span>
              <input
                class="input"
                type="number"
                name="hour"
                placeholder="Jam"
                min="0"
                max="23"
                required
                v-model="scheduleTime.hour"
                style="text-align: center;"
              >
              <span class="field-input-addons">:</span>
              <input
                class="input"
                type="number"
                name="minute"
                placeholder="Menit"
                min="0"
                max="59"
                required
                v-model="scheduleTime.minute"
                style="text-align: center;"
              >
            </div>
          </div>
          <div class="field">
            <div class="field-input">
              <div class="input-checkbox-group horizontal">

                <label class="input-checkbox">
                  <input
                    type="checkbox"
                    name="relay_pins[]"
                    value="<?= $speaker_pins[0] ?>"
                    v-model="scheduleTime.relayPins"
                  >
                  <span class="input-checkbox-label">B1 (Mixer)</span>
                </label>

                <label class="input-checkbox">
                  <input
                    type="checkbox"
                    name="relay_pins[]"
                    value="<?= $speaker_pins[1] ?>"
                    v-model="scheduleTime.relayPins"
                  >
                  <span class="input-checkbox-label">B2 (Ampli 1)</span>
                </label>

                <label class="input-checkbox">
                  <input
                    type="checkbox"
                    name="relay_pins[]"
                    value="<?= $speaker_pins[2] ?>"
                    v-model="scheduleTime.relayPins"
                  >
                  <span class="input-checkbox-label">B3 (Ampli 2)</span>
                </label>

              </div>
            </div>
          </div>

          <div
            style="
              margin: 1rem .75rem;
              display: flex;
              justify-content: space-between;
              align-items: center;
            "
          >
            <h5
              style="
                user-select: none;
                -moz-user-select: none;
                font-size: 1.25rem;
                font-weight: 400;
                margin: 0;
                color: #424242;
              "
            >Materi Audio<br><small style="font-size: 1rem; font-style: italic; color: #424242;">total durasi: {{ durationFormat(scheduleTime.audios.map(audio => audio.duration).reduce((a, b) => a + b, 0)) }}</small></h5>
            <button type="button" class="button outline" @click="modalLibrary = true;">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-library_music-24px.svg" ?>
              </span>
              <span class="text">Tambah Audio</span>
            </button>
          </div>
          <p
            v-if="scheduleTime.audios.length === 0"
            class="notification has-text-centered is-size-7"
            style="margin: .75rem; color: #424242; font-size: .96rem;"
          >Materi audio belum ditambahkan.</p>
          <ul class="list-view">
            <li
              v-for="(audio, index) in scheduleTime.audios"
              :key="index + audio.hash"
              class="list-view-item"
            >
              <input type="hidden" name="audios[]" :value="audio.hash">
              <div class="list-view-item-row">
                <div class="list-view-item-title">
                  <span class="flex-ellipsis">{{ audio.filename }}</span>
                </div>
                <button
                  type="button"
                  class="button small secondary"
                  @click="playAudio(audio);"
                >
                <span class="icon">
                    <?php include __DIR__ . "/../static/icons/round-play_arrow-24px.svg" ?>
                  </span>
                </button>
                <button
                  type="button"
                  class="button small secondary"
                  @click="moveAudioPosition(audio, -1);"
                >
                  <span class="icon">
                    <?php include __DIR__ . "/../static/icons/round-arrow_upward-24px.svg" ?>
                  </span>
                </button>
                <button
                  type="button"
                  class="button small secondary"
                  @click="moveAudioPosition(audio, 1);"
                >
                  <span class="icon">
                    <?php include __DIR__ . "/../static/icons/round-arrow_downward-24px.svg" ?>
                  </span>
                </button>
                <button
                  type="button"
                  class="button small danger"
                  @click="scheduleTime.audios.splice(scheduleTime.audios.indexOf(audio), 1)"
                >
                  <span class="icon">
                    <?php include __DIR__ . "/../static/icons/outline-delete_forever-24px.svg" ?>
                  </span>
                </button>
              </div>
            </li>
          </ul>
        </div>
        <footer class="modal-footer">
          <div class="modal-footer-left">
            <button
              v-show="!isNewTime"
              type="button"
              class="button danger"
              @click="modalScheduleTimeDelete = true"
            >
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-delete_forever-24px.svg" ?>
              </span>
              <span class="text">Hapus Jadwal</span>
            </button>
          </div>
          <div class="modal-footer-right">
            <button type="submit" class="button primary">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-save-24px.svg" ?>
              </span>
              <span class="text">Simpan</span>
            </button>
          </div>
        </footer>
      </form>
    </dialog>
    </transition>
    <!-- /SCHEDULE_TIME -->

    <!-- LIBRARY -->
    <transition name="fade">
    <dialog v-if="modalLibrary" class="modal" open>
      <div class="modal-overlay"></div>
      <div class="modal-shell">
        <header class="modal-header">
          <div class="modal-header-row">
            <div class="modal-header-left">
              <h4 class="modal-title">Pilih Materi Audio</h4>
            </div>
            <div class="modal-header-right">
              <button type="button" class="button secondary" @click="modalLibrary = false">
                <span class="icon">
                  <?php include __DIR__ . "/../static/icons/round-close-24px.svg" ?>
                </span>
              </button>
            </div>
          </div>
          <div class="modal-header-row" style="align-items: flex-start;">
            <div class="field">
              <div class="field-input">
                <span class="field-input-addons">
                  <span class="icon">
                    <?php include __DIR__ . "/../static/icons/round-search-24px.svg" ?>
                  </span>
                </span>
                <input type="text" placeholder="Pencarian" v-model="selectorKeyword">
              </div>
            </div>
          </div>
        </header>
        <div class="modal-content">
          <p
            v-if="isEmpty"
            class="notification"
          >Belum ada audio dalam perangkat Awqot. Silahkan upload audio terlebih dahulu.</p>
          <p
            v-if="isNotFound"
            class="notification"
          >Tidak ditemukan materi dengan kata kunci "{{ selectorKeyword }}".</p>
          <ul class="list-view selectable">
            <li
              v-for="audio in filteredAudios"
              :key="audio.hash"
              class="list-view-item ellipsis"
              :class="selectedAudios.indexOf(audio) !== -1 && 'selected'"
              style="max-width: unset;"
              @click="toggleSelectAudio(audio)"
            >
              <div class="list-view-item-row">
                <div class="list-view-item-title">
                  <span
                    class="flex-ellipsis"
                    :style="audio.isNew && 'color: #1B5E20;'"
                  >{{ audio.filename }}</span>
                  <span style="color: #616161;">{{ durationFormat(audio.duration) }}</span>
                </div>
              </div>
            </li>
          </ul>
        </div>
        <footer class="modal-footer">
          <div class="modal-footer-left">
          </div>
          <div class="modal-footer-right">
            <form
              ref="audio_upload_form"
              action="../api/audio_upload.php"
              method="post"
              enctype="multipart/form-data"
            >
              <input
                ref="audio_upload_input"
                type="file"
                name="audio_files[]"
                accept="audio/*"
                multiple="multiple"
                style="display: none;"
                @input="uploadAudios"
              >
            </form>
            <button
              type="button"
              class="button secondary"
              @click="$refs.audio_upload_input.click()"
              :disabled="isUploading"
            >
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-cloud_upload-24px.svg" ?>
              </span>
              <span class="text">{{ isUploading ? 'Mengirim...' : 'Upload' }}</span>
            </button>
            <button type="button" class="button primary" @click="selectAudioDone()">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-check-24px.svg" ?>
              </span>
              <span class="text">Tambahkan</span>
            </button>
          </div>
        </footer>
      </div>
    </dialog>
    </transition>
    <!-- /LIBRARY -->

    <!-- SCHEDULE_TIME_DELETE -->
    <transition name="fade">
    <dialog v-if="modalScheduleTimeDelete" class="modal" open>
      <div class="modal-overlay"></div>
      <form class="modal-shell" method="post">
        <div class="modal-content">
          <input type="hidden" name="action" value="delete_schedule_time">
          <input type="hidden" name="id" :value="scheduleTime.id">
          <p>Apakah anda yakin menghapus jadwal ini?</p>
        </div>
        <footer class="modal-footer">
          <div class="modal-footer-left">
            <button type="button" class="button secondary" @click="modalScheduleTimeDelete = false">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-close-24px.svg" ?>
              </span>
              <span class="text">Batal</span>
            </button>
          </div>
          <div class="modal-footer-right">
            <button type="submit" class="button danger">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-delete_forever-24px.svg" ?>
              </span>
              <span class="text">Hapus</span>
            </button>
          </div>
        </form>
      </footer>
    </dialog>
    </transition>
    <!-- /SCHEDULE_TIME_DELETE -->

    <!-- SCHEDULE_DAY_DUPLICATE -->
    <transition name="fade">
    <dialog v-if="modalDuplicate" class="modal" open>
      <div class="modal-overlay"></div>
      <form class="modal-shell" method="post">
        <header class="modal-header">
          <div class="modal-header-row">
            <h3 class="modal-title">Duplikat Hari {{ activeDay.label }} ke Hari?</h3>
          </div>
        </header>
        <div class="modal-content">
          <input type="hidden" name="action" value="duplicate_days">
          <input type="hidden" name="source_day_id" :value="activeDay.id">
          <ul class="list-view selectable">
            <li
              v-for="day in days.filter(day => day !== activeDay)"
              :key="day.id"
              class="list-view-item"
              :class="selectedDays.indexOf(day) !== -1 && 'selected'"
              @click="toggleSelectDay(day)"
            >
              <div class="list-view-item-row">
                <input
                  v-if="selectedDays.indexOf(day) !== -1"
                  type="hidden"
                  name="target_day_ids[]"
                  :value="day.id"
                >
                <span class="list-view-item-title">{{ day.label }}</span>
              </div>
            </li>
          </ul>
        </div>
        <footer class="modal-footer">
          <div class="modal-footer-left">
            <button type="button" class="button secondary" @click="modalDuplicate = false">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-close-24px.svg" ?>
              </span>
              <span class="text">Batal</span>
            </button>
          </div>
          <div class="modal-footer-right">
            <button type="submit" class="button primary">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-file_copy-24px.svg" ?>
              </span>
              <span class="text">Duplikat</span>
            </button>
          </div>
        </form>
      </footer>
    </dialog>
    </transition>
    <!-- /SCHEDULE_DAY_DUPLICATE -->
  </div>

  <?php export_to_js("audios", $audios); ?>
  <?php export_to_js("schedule", $schedule); ?>
  <?php export_to_js("schedule_times", $schedule_times); ?>

  <script src="/static/scripts/vue.js"></script>
  <script><?php require_once __DIR__ . "/schedule.js" ?></script>
</body>

</html>
