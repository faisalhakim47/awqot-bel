<?php

require_once __DIR__ . "/../api/commands/awqot_ensure_upgrade.php";
require_once __DIR__ . "/../api/tools/configuration.php";

$rpi_serial_number = exec("cat /proc/cpuinfo | grep Serial | awk ' {print $3}'");
$product_key = get_configuration("product_key");

// ---------- VIEW ----------

require_once __DIR__ . "/../components/basic_style.php";
require_once __DIR__ . "/../components/box.php";
require_once __DIR__ . "/../components/button.php";
require_once __DIR__ . "/../components/button_group.php";
require_once __DIR__ . "/../components/event.php";
require_once __DIR__ . "/../components/field.php";
require_once __DIR__ . "/../components/head.php";
require_once __DIR__ . "/../components/http.php";
require_once __DIR__ . "/../components/icon.php";
require_once __DIR__ . "/../components/notification.php";
require_once __DIR__ . "/../components/page.php";
require_once __DIR__ . "/../components/sidebar.php";

?><!DOCTYPE html>

<html lang="id">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Data Pengguna - Awqot</title>
  <?php publish("head"); ?>
</head>

<body>
  <?php publish("body"); ?>
  <div id="app" class="page" style="padding-top: 3.5rem;">
    <header class="page-header">
      <div class="page-header-row">
        <div class="page-header-left">
          <h1 class="page-title">Data Pengguna Awqot</h1>
        </div>
      </div>
    </header>

    <main class="page-content padded">
      <section class="box">
        <header class="box-header">
          <h2 class="box-title">Data Pengguna</h2>
        </header>
        <form
          id="register_form"
          class="box-content"
          enctype="multipart/form-data"
          action="https://admin.awqot.com/api/registerAwqotOwnership"
          onsubmit="registerSubmit(this, event);"
        >
          <input type="hidden" name="rpiSerialNumber" value="<?= $rpi_serial_number ?>">
          <input type="hidden" name="productKey" value="<?= $product_key ?>">
          <input type="hidden" name="organizationType" value="bel">
          <div class="field">
            <div class="field-label">
              <label for="register_name">Nama Pengurus</label>
            </div>
            <div class="field-input">
              <input
                id="register_name"
                type="text"
                name="name"
                autocomplete="name"
                placeholder="nama lengkap"
                required
              >
            </div>
          </div>
          <div class="field">
            <div class="field-label">
              <label for="register_phoneNumber">Nomor Telepon Pengurus</label>
            </div>
            <div class="field-input">
              <input
                id="register_phoneNumber"
                type="tel"
                name="phoneNumber"
                autocomplete="tel"
                placeholder="nomor telepon atau nomor whatsapp"
                required
              >
            </div>
            <p class="field-info">Disarankan untuk mencantumkan nomor yang aktif WhatsApp.</p>
          </div>
          <div class="field">
            <div class="field-label">
              <label for="register_organizationName">Nama Institusi</label>
            </div>
            <div class="field-input">
              <input
                id="register_organizationName"
                type="text"
                name="organizationName"
                placeholder="nama institusi yang dipasang Awqot"
                required
              >
            </div>
          </div>
          <div class="field">
            <div class="field-label">
              <label for="register_address">Alamat Institusi</label>
            </div>
            <div class="field-input">
              <textarea
                id="register_address"
                type="text"
                name="address"
                autocomplete="shipping street-address"
                placeholder="alamat institusi tempat dipasang Awqot"
                required
              ></textarea>
            </div>
          </div>
          <div class="field">
            <div class="field-label">
              <label for="register_photo">Foto Institusi</label>
            </div>
            <div class="field-input">
              <input id="register_photo" type="file" name="photo" accept="image/*">
            </div>
          </div>
          <div class="button-group align-right">
            <button id="register_submit_button" type="submit" class="button primary solid">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-save-24px.svg" ?>
              </span>
              <span id="register_submit_text" class="text">Simpan</span>
            </button>
          </div>
        </form>
      </section>
    </main>
  </div>
  <script>
  function registerSubmit(form, event) {
    event.preventDefault();
    var originalSubmitText = register_submit_text.textContent;
    register_submit_button.disabled = true;
    register_submit_text.textContent = 'Menyimpan...';
    var data = new FormData(form)
    Promise.all([
      http.request('POST', 'https://admin.awqot.com/api/registerAwqotOwnership', { data: data }),
      http.request('POST', '/api/awqot_ownership_set.php', { data: data }),
    ]).then(function () {
      register_submit_text.textContent = 'Tersimpan.';
      alert('Terimakasih telah melakukan pendataan.');
      window.location.pathname = '/';
    }).catch(function () {
      console.warn(error);
      alert('Gagal');
      register_submit_button.disabled = false;
      register_submit_text.textContent = originalSubmitText;
    });
  }
  </script>
</body>

</html>
