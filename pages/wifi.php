<?php

require_once __DIR__ . "/../api/commands/awqot_ensure_upgrade.php";
require_once __DIR__ . "/../api/tools/database.php";
require_once __DIR__ . "/../api/tools/child_process.php";

// ----- CONTROLLER -----

if (isset($_POST["action"])) switch ($_POST["action"]) {
  case "wifi_set":
  $is_hotspot = exec("systemctl is-active hostapd") === "active";
  $ssid = $_POST["ssid"];
  $passphrase = $_POST["passphrase"];
  $hostapd = escapeshellarg(str_replace("  ", "", "
    interface=wlan0
    driver=nl80211
    ssid={$ssid}
    hw_mode=g
    channel=7
    auth_algs=1
    wpa=2
    wpa_passphrase={$passphrase}
    wpa_key_mgmt=WPA-PSK
    wpa_pairwise=TKIP
    rsn_pairwise=CCMP
  "));
  $wpa_supplicant = escapeshellarg(str_replace("  ", "", '
    ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
    update_config=1
    network={
      ssid="'. $ssid .'"
      psk="'. $passphrase .'"
    }
  '));
  exec("echo {$hostapd} | sudo tee /etc/hostapd/hostapd.conf > /dev/null");
  exec("echo {$wpa_supplicant} | sudo tee /etc/wpa_supplicant/wpa_supplicant.conf > /dev/null");
  switch ($_POST["mode"]) {
    case "hotspot":
    $dhcpcd_hotspot = escapeshellarg(str_replace("  ", "", "
      hostname
      clientid
      persistent
      option rapid_commit
      option domain_name_servers, domain_name, domain_search, host_name
      option classless_static_routes
      option ntp_servers
      option interface_mtu
      require dhcp_server_identifier
      slaac private
      interface usb0
      static ip_address=192.168.42.42/24
      static routers=192.168.42.129
      static domain_name_servers=192.168.42.129 8.8.8.8
      interface wlan0
      static ip_address=192.168.1.1/24
      nohook wpa_supplicant
    "));
    $interfaces_hotspot = escapeshellarg(str_replace("  ", "", "
      source-directory /etc/network/interfaces.d
    "));
    exec("echo {$dhcpcd_hotspot} | sudo tee /etc/dhcpcd.conf > /dev/null");
    exec("echo {$interfaces_hotspot} | sudo tee /etc/network/interfaces > /dev/null");
    exec("sudo systemctl restart networking");
    exec("sudo systemctl enable dnsmasq");
    exec("sudo systemctl enable hostapd");
    exec("sudo systemctl restart dnsmasq");
    exec("sudo systemctl restart hostapd");
    break;
    case "wifi":
    $dhcpcd_wifi = escapeshellarg(str_replace("  ", "", "
      hostname
      clientid
      persistent
      option rapid_commit
      option domain_name_servers, domain_name, domain_search, host_name
      option classless_static_routes
      option ntp_servers
      option interface_mtu
      require dhcp_server_identifier
      slaac private
      interface usb0
      static ip_address=192.168.42.42/24
      static routers=192.168.42.129
      static domain_name_servers=192.168.42.129 8.8.8.8
    "));
    $interfaces_wifi = escapeshellarg(str_replace("  ", "", "
      source-directory /etc/network/interfaces.d
      auto wlan0
      iface wlan0 inet manual
      wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf
    "));
    exec("echo {$dhcpcd_wifi} | sudo tee /etc/dhcpcd.conf > /dev/null");
    exec("echo {$interfaces_wifi} | sudo tee /etc/network/interfaces > /dev/null");
    exec("sudo systemctl disable hostapd");
    exec("sudo systemctl disable dnsmasq");
    exec("sudo systemctl stop hostapd");
    exec("sudo systemctl stop dnsmasq");
    exec("sudo systemctl restart networking");
    exec("sudo ifup wlan0");
    exec("sudo wpa_cli -i wlan0 reconfigure");
    break;
  }
  break;
}

if ($_SERVER["REQUEST_METHOD"] === "POST") {
  header("Location: {$_SERVER["REQUEST_URI"]}");
  exit();
}

// ----- MODEL -----

$is_hotspot = exec("systemctl is-active hostapd") === "active";
$ssid = "";
$passphrase = "";

foreach (explode("\n", file_get_contents("/etc/hostapd/hostapd.conf")) as $property)  {
  $property = explode("=", $property);
  if ($property[0] === "ssid") $ssid = $property[1];
  if ($property[0] === "wpa_passphrase") $passphrase = $property[1];
};

// ----- VIEW -----

require_once __DIR__ . "/../components/basic_style.php";
require_once __DIR__ . "/../components/box.php";
require_once __DIR__ . "/../components/button.php";
require_once __DIR__ . "/../components/button_group.php";
require_once __DIR__ . "/../components/field.php";
require_once __DIR__ . "/../components/head.php";
require_once __DIR__ . "/../components/icon.php";
require_once __DIR__ . "/../components/notification.php";
require_once __DIR__ . "/../components/page.php";
require_once __DIR__ . "/../components/sidebar.php";

?><!DOCTYPE html>

<html lang="id">

<head>
  <title>Koneksi - Awqot</title>
  <?php publish("head"); ?>
</head>

<body>
  <?php publish("body"); ?>
  <div id="app" class="page" style="padding-top: 3.5rem;">
    <header class="page-header">
      <div class="page-header-row">
        <div class="page-header-left">
          <button type="button" class="button" onclick="ev.publish('sidebar:open');">
            <span class="icon">
              <?php include __DIR__ . "/../static/icons/round-menu-24px.svg"; ?>
            </span>
          </button>
          <h1 class="page-title">Pengaturan Wifi</h1>
        </div>
      </div>
    </header>

    <main class="page-content padded">
      <section class="box">
        <header class="box-header">
          <h2 class="box-title">Wifi dan Password</h2>
        </header>
        <form class="box-content" method="post">
          <input type="hidden" name="action" value="wifi_set">
          <div class="field">
            <div class="field-label">
              <label for="ssid_input">Mode Awqot</label>
            </div>
            <div class="field-input">
              <select name="mode" id="connection_mode">
                <option value="hotspot" <?= $is_hotspot ? "selected" : "" ?>>Pemancar</option>
                <option value="wifi" <?= $is_hotspot ? "" : "selected" ?>>Penerima</option>
              </select>
            </div>
          </div>
          <div class="field">
            <div class="field-label">
              <label for="ssid_input">Nama Wifi (SSID)</label>
            </div>
            <div class="field-input">
              <input id="ssid_input" type="text" name="ssid" value="<?= $ssid ?>">
            </div>
          </div>
          <div class="field">
            <div class="field-label">
              <label for="ssid_input">Password Wifi</label>
            </div>
            <div class="field-input">
              <input id="ssid_input" type="text" name="passphrase" value="<?= $passphrase ?>">
            </div>
          </div>
          <div class="button-group align-right">
            <button type="submit" class="button primary solid">
              <span class="icon">
                <?php include __DIR__ . "/../static/icons/round-save-24px.svg" ?>
              </span>
              <span class="text">Terapkan</span>
            </button>
          </div>
        </form>
      </section>
    </main>
  </div>
</body>

</html>
