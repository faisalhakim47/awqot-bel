#!/usr/bin/env bash

sudo rm -f /etc/apache2/sites-enabled/*
sudo ln -s /etc/apache2/sites-available/awqot-bel.conf /etc/apache2/sites-enabled/awqot.conf
sudo ln -s /etc/apache2/sites-available/phpmyadmin.conf /etc/apache2/sites-enabled/phpmyadmin.conf
sudo systemctl restart mysql apache2
