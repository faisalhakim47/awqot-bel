<?php

require_once __DIR__ . "/event.php";
require_once __DIR__ . "/http.php";

subscribe("head", function () {
  echo "<script>";
  require_once __DIR__ . "/check_ds3231.js";
  echo "</script>";
});

subscribe("body", function () {
  require_once __DIR__ . "/check_ds3231.html";
});
