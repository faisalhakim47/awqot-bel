<?php

require_once __DIR__ . "/animation.php";
require_once __DIR__ . "/basic_style.php";
require_once __DIR__ . "/event.php";

subscribe("head", function () {
  echo "<script>";
  require_once __DIR__ . "/page_transition.js";
  echo "</script>";
});

subscribe("body", function () {
  require_once __DIR__ . "/page_transition.html";
});
