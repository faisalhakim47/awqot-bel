var awqotAdminData = JSON.parse(document.getElementById('awqot_admin_data').textContent);

var data = new FormData();
data.append('rpiSerialNumber', awqotAdminData.rpiSerialNumber);
data.append('productKey', awqotAdminData.productKey || 'NOT_REGISTERED');

function checkRegistration() {
  return http.request('POST', 'https://admin.awqot.com/api/checkAwqotRegistration', { data: data })
    .catch(function (response) {
      var result = response.result || {};
      if (result.code === 'PRODUCT_KEY_INVALID' && confirm('Product Key is Invalid. Change it?')) {
        var productKey = generateProductKey();
        var adminData = new FormData();
        adminData.append('rpiSerialNumber', awqotAdminData.rpiSerialNumber);
        adminData.append('oldProductKey', prompt('Input old product key!'));
        adminData.append('productKey', productKey);
        var awqotData = new FormData();
        awqotData.append('product_key', productKey);
        return Promise.all([
          http.request('POST', 'https://admin.awqot.com/api/changeProductKey', { data: adminData }),
          http.request('POST', '/api/awqot_product_key_set.php', { data: awqotData }),
        ]).then(function () {
          ev.publish('modal:close', { dialog: awqot_admin_register_unit });
          return checkRegistration();
        });
      } else {
        alert(JSON.stringify(result));
        throw new Error(response);
      }
    })
    .then(function (result) {
      if (!result.isRegistered) {
        if (confirm('daftarkan unit awqot?')) {
          ev.publish('modal:open', { dialog: awqot_admin_register_unit });
          var productKey = generateProductKey();
          var adminData = new FormData();
          adminData.append('type', 'bel');
          adminData.append('rpiSerialNumber', awqotAdminData.rpiSerialNumber);
          adminData.append('productKey', productKey);
          var awqotData = new FormData();
          awqotData.append('product_key', productKey);
          return Promise.all([
            http.request('POST', 'https://admin.awqot.com/api/registerAwqot', { data: adminData }),
            http.request('POST', '/api/awqot_product_key_set.php', { data: awqotData }),
          ]).then(function () {
            ev.publish('modal:close', { dialog: awqot_admin_register_unit });
            return result;
          });
        } else {
          throw new Error('unit not registered');
        }
      }
    })
    .then(function (result) {
      ev.publish('awqot_admin:ready');
      return result;
    });
}

function generateProductKey() {
  var result = '';
  var randomInts = new Uint32Array(8);
  window.crypto.getRandomValues(randomInts);
  for (var index = 0; index < randomInts.length; index++) {
    result += randomInts[index].toString(36);
  }
  return result;
}

window.addEventListener('DOMContentLoaded', checkRegistration);
window.addEventListener('DOMContentLoaded', function () {
  http.request('GET', '/api/commands/awqot_task_handler.php');
});
