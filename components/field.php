<?php

require_once __DIR__ . "/basic_style.php";
require_once __DIR__ . "/event.php";

subscribe("head", function () {
  echo "<style>";
  require_once __DIR__ . "/field.css";
  echo "</style>";
});
