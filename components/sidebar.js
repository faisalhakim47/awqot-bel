document.addEventListener('DOMContentLoaded', function () {
  var isSidebarVisible = false;
  var touchstart = null;
  var isSliding = false;
  var sidebar = document.getElementById('sidebar');
  var sidebarList = sidebar.querySelector('.sidebar-list');

  document.addEventListener('touchstart', function (_touchstart) {
    touchstart = _touchstart;
    var x = _touchstart.changedTouches[0].clientX
    isSliding = isSidebarVisible ? x > 220 : x < 75;
    if (isSliding) sidebarList.style.transitionDuration = '0ms';
  });

  document.addEventListener('touchmove', function (touchmove) {
    if (!isSliding) return;
    else requestAnimationFrame(function () {
      touchmove = touchmove.changedTouches[0];
      var x = touchmove.clientX - 220;
      sidebarList.style.transform = 'translateX(' + (x > 0 ? 0 : x) + 'px)';
    });
  });

  document.addEventListener('touchend', function (touchend) {
    if (!isSliding) return;
    var xStart = touchstart.changedTouches[0].clientX;
    var xEnd = touchend.changedTouches[0].clientX;
    var xDiff = xEnd - xStart;
    var timeDiff = touchend.timeStamp - touchstart.timeStamp;
    var willSidebarVisible = isSidebarVisible
      ? !(xDiff < -125 || (xDiff < -75 && timeDiff < 200) || (xDiff === 0, xStart > 220))
      : xDiff > 125 || (xDiff > 75 && timeDiff < 200);
    requestAnimationFrame(function () {
      sidebarList.style.transform = null;
      sidebarList.style.transitionDuration = null;
    });
    if (willSidebarVisible) ev.publish('sidebar:open');
    else ev.publish('sidebar:close');
  });

  ev.subscribe('sidebar:open', function open() {
    if (isSidebarVisible) return;
    isSidebarVisible = true;
    var sidebar = document.getElementById('sidebar');
    var overlay = sidebar.querySelector('.sidebar-overlay');
    sidebar.classList.add('sidebar-visible');
    ev.publish('animate', {
      duration: 300,
      transition: function fadeIn(progress) {
        if (progress === 0) overlay.style.display = 'block';
        overlay.style.opacity = progress;
      },
    });
  });

  ev.subscribe('sidebar:close', function close() {
    if (!isSidebarVisible) return;
    isSidebarVisible = false;
    var sidebar = document.getElementById('sidebar');
    var overlay = sidebar.querySelector('.sidebar-overlay');
    sidebar.classList.remove('sidebar-visible');
    ev.publish('animate', {
      duration: 300,
      transition: function fadeOut(progress) {
        overlay.style.opacity = 1 - progress;
        if (progress === 1) overlay.style.display = 'none';
      },
    });
  });
});
