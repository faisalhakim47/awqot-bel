var awqotAdminData = JSON.parse(document.getElementById('awqot_admin_data').textContent);

ev.subscribe('awqot_admin:ready', function () {
  var data = new FormData();
  data.append('rpiSerialNumber', awqotAdminData.rpiSerialNumber);
  data.append('productKey', awqotAdminData.productKey);
  http.request('POST', 'https://admin.awqot.com/api/checkAwqotOwnership', { data: data })
    .then(function (result) {
      if (!result.hasOwnerData) {
        ev.publish('modal:open', { dialog: register_notice });
      }
    });
});
