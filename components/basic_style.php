<?php

require_once __DIR__ . "/event.php";
require_once __DIR__ . "/typography.php";

subscribe("head", function () {
  echo "<style>";
  require_once __DIR__ . "/basic_style.css";
  echo "</style>";
});
