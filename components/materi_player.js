document.addEventListener('DOMContentLoaded', function () {
  var audio_hash = '';
  var isPlaying = false;
  var mediaPlayer = 'awqot';
  var slider = document.getElementById('materi_player_slider');
  slider.min = 0;
  slider.max = 100;
  slider.value = 0;
  var devicePlayer = document.createElement('audio');
  devicePlayer.style.display = 'none';
  document.body.appendChild(devicePlayer);
  var durationHolder = document.getElementById('materi_player_duration');
  var currentTimeHolder = document.getElementById('materi_player_currenttime');

  function toTwoDigit(number) {
    return number < 10 ? '0' + number.toString() : number;
  }

  function secondsToTime(seconds) {
    return toTwoDigit(Math.floor(seconds / 60)) + ':'
      + toTwoDigit(Math.round(seconds % 60));
  }

  setInterval(function () {
    if (isPlaying) {
      var nextValue = parseInt(slider.value) + 500;
      var duration = parseInt(slider.max) / 1000;
      slider.value = nextValue;
      requestAnimationFrame(function () {
        currentTimeHolder.textContent = secondsToTime(nextValue / 1000);
        durationHolder.textContent = secondsToTime(duration);
      });
    }
  }, 500);

  devicePlayer.addEventListener('durationchange', function () {
    mediaPlayer = 'device';
    slider.value = 0;
    slider.max = devicePlayer.duration * 1000;
    isPlaying = true;
    devicePlayer.play();
  });

  devicePlayer.addEventListener('ended', function () {
    ev.publish('modal:close', { dialog: materi_player });
  });

  slider.addEventListener('change', function () {
    ev.publish('materi_player:seek', {
      seconds: Math.round(parseInt(slider.value, 10) / 1000)
    });
  });

  ev.subscribe('materi_player:open', function materiPlayerOpen(data) {
    audio_hash = data.audio_hash;
    ev.publish('modal:open', { dialog: materi_player_prompt });
  });

  ev.subscribe('materi_player:play_on_device', function materiPlayerPlayOnDevice() {
    ev.publish('modal:close', { dialog: materi_player_prompt });
    ev.publish('modal:open', { dialog: materi_player });
    devicePlayer.src = '/data/audios/' + audio_hash;
  });

  ev.subscribe('materi_player:speaker_switch', function ({ speaker, condition }) {
    var request = new XMLHttpRequest();
    request.open('GET', '/api/commands/switch_pin.php?pin=' + window.speakerPins[speaker] + '&condition=' + condition);
    request.send();
  });

  ev.subscribe('materi_player:play_on_awqot', function materiPlayerPlayOnAwqot() {
    var buttons = document.querySelectorAll('#materi_player_prompt button');
    var button = buttons.item(0);
    requestAnimationFrame(function () {
      for (var index = 0; index < buttons.length; index++) {
        button = buttons.item(index);
        button.disabled = true;
      }
    });
    var request = new XMLHttpRequest();
    request.open('GET', '/api/audio_play.php?audio_hash=' + audio_hash + '&start_time=0', true);
    request.addEventListener('readystatechange', function () {
      if (request.readyState === request.DONE) {
        if (request.status === 200) {
          isPlaying = true;
          mediaPlayer = 'awqot';
          ev.publish('modal:close', { dialog: materi_player_prompt });
          ev.publish('modal:open', { dialog: materi_player });
          var playerData = request.responseText.split(' ');
          slider.max = parseInt(playerData[1], 10);
          slider.value = 0;
        }
        requestAnimationFrame(function () {
          for (var index = 0; index < buttons.length; index++) {
            button = buttons.item(index);
            button.disabled = false;
          }
        });
      }
    });
    request.send();
  });

  ev.subscribe('materi_player:seek', function materiPlayerSeek(data) {
    if (mediaPlayer === 'awqot') {
      var buttons = document.querySelectorAll('#materi_player button');
      var button = buttons.item(0);
      requestAnimationFrame(function () {
        for (var index = 0; index < buttons.length; index++) {
          button = buttons.item(index);
          button.disabled = true;
        }
      });
      var request = new XMLHttpRequest();
      request.open('GET', '/api/audio_seek.php?seconds=' + data.seconds, true);
      request.addEventListener('readystatechange', function () {
        if (request.readyState === request.DONE) {
          requestAnimationFrame(function () {
            for (var index = 0; index < buttons.length; index++) {
              button = buttons.item(index);
              button.disabled = false;
            }
          });
        }
      });
      request.send();
    }
    else if (mediaPlayer === 'device') {
      devicePlayer.currentTime = data.seconds;
    }
  });

  ev.subscribe('materi_player:stop', function materiPlayerStop() {
    if (mediaPlayer === 'awqot') {
      var request = new XMLHttpRequest();
      var buttons = document.querySelectorAll('#materi_player button');
      var button = buttons.item(0);
      requestAnimationFrame(function () {
        for (var index = 0; index < buttons.length; index++) {
          button = buttons.item(index);
          button.disabled = true;
        }
      });
      request.open('GET', '/api/audio_stop.php', true);
      request.addEventListener('readystatechange', function () {
        if (request.readyState === request.DONE) {
          if (request.status === 200) {
            ev.publish('modal:close', { dialog: materi_player });
          }
          requestAnimationFrame(function () {
            for (var index = 0; index < buttons.length; index++) {
              button = buttons.item(index);
              button.disabled = false;
            }
          });
        }
      });
      request.send();
    } else if (mediaPlayer === 'device') {
      devicePlayer.pause();
      ev.publish('modal:close', { dialog: materi_player });
    }
    mediaPlayer = 'stopped';
  });
});
