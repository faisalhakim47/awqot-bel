<?php

require_once __DIR__ . "/event.php";
require_once __DIR__ . "/field.php";
require_once __DIR__ . "/modal.php";

subscribe("head", function () {
  echo "<script>";
  require_once __DIR__ . "/awqot_time.js";
  echo "</script>";
});

subscribe("body", function () {
  ?>
<script>
  var awqotTime = <?= round(microtime(true) * 1000); ?>;
  var deviceTime = new Date().getTime();
  var latency = deviceTime - performance.timing.responseStart;
  var awqotTimeDiff = (awqotTime + latency) - deviceTime;
</script>
<dialog id="awqot_time" class="modal">
  <form class="modal-shell" method="post">
    <header class="modal-header">
      <div class="modal-header-row">
        <div class="modal-header-left" style="flex: 1;">
          <h4 class="modal-title" style="text-align: center;">Sesuaikan Waktu</h4>
        </div>
      </div>
    </header>
    <div class="modal-content">
      <input type="hidden" name="action" value="playlist_create">
      <p style="text-align: center;">Waktu dan Tanggal pada Awqot belum tepat.</p>
      <table class="table">
        <tbody>
          <tr>
            <td><strong>Pada Awqot</strong></td>
            <td
              rowspan="2"
              style="text-align: center; border-right: 1px solid #e0e0e0; border-left: 1px solid #e0e0e0;"
            >
              <span class="icon" style="width: 2.5rem; height: 2.5rem;">
                <?php include __DIR__ . "/../static/icons/round-arrow_right_alt-24px.svg" ?>
              </span>
            </td>
            <td><strong>Saat ini</strong></td>
          </tr>
          <tr>
            <td id="awqot_time_before"></td>
            <td id="awqot_time_after"></td>
          </tr>
        </tbody>
      </table>
    </div>
    <footer class="modal-footer">
      <div class="modal-footer-left">
        <button
          type="button"
          class="button secondary"
          onclick="ev.publish('modal:close', { dialog: awqot_time });"
        >
          <span class="icon">
            <?php include __DIR__ . "/../static/icons/round-close-24px.svg" ?>
          </span>
          <span class="text">Abaikan</span>
        </button>
      </div>
      <div class="modal-footer-right">
        <button type="button" class="button primary" onclick="ev.publish('awqot:set_time');">
          <span class="icon">
            <?php include __DIR__ . "/../static/icons/round-save-24px.svg" ?>
          </span>
          <span class="text">Sesuaikan</span>
        </button>
      </div>
    </footer>
  </form>
</dialog>
  <?php
});
