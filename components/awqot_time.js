document.addEventListener('DOMContentLoaded', function () {
  if (awqotTimeDiff < -15000 || awqotTimeDiff > 15000) {
    function toTwoDigit(number) {
      number = number.toString();
      return number < 10 ? '0' + number : number;
    }
    function humanizeDate(date) {
      return toTwoDigit(date.getFullYear())
      + '-' + toTwoDigit(date.getMonth() + 1)
      + '-' + toTwoDigit(date.getDate())
      + ' ' + toTwoDigit(date.getHours())
      + ':' + toTwoDigit(date.getMinutes());
    }
    awqot_time_before.textContent = humanizeDate(new Date(awqotTime));
    awqot_time_after.textContent = humanizeDate(new Date(deviceTime));
    if (localStorage.getItem('lastAwqotTime') == awqotTime) {
      window.location.reload();
    } else {
      ev.publish('modal:open', { dialog: awqot_time });
    }
  }

  localStorage.setItem('lastAwqotTime', awqotTime);

  ev.subscribe('awqot:set_time', function awqotSetTime() {
    var request = new XMLHttpRequest();
    var buttons = document.querySelectorAll('#awqot_time button');
    for (var index = 0; index < buttons.length; index++) {
      buttons[index].disabled = true;
    }
    request.open('POST', '/api/time_set.php?time=' + Math.round(new Date().getTime() / 1000), true);
    request.addEventListener('readystatechange', function () {
      if (request.readyState === request.DONE) {
        if (request.status === 200) location.reload();
        else for (var index = 0; index < buttons.length; index++) {
          buttons[index].disabled = false;
        }
      }
    });
    request.send();
  });
});
