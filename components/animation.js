ev.subscribe('animate', function animate(data) {
  var start = 0;
  var step = function step(timestamp) {
    if (start === 0) start = timestamp;
    var progress = (timestamp - start) / data.duration;
    progress = progress >= 1 ? 1 : progress;
    data.transition(progress);
    if (progress !== 1) requestAnimationFrame(step);
  };
  requestAnimationFrame(step);
});
