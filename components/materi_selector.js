document.addEventListener('DOMContentLoaded', function () {
  new Jets({
    searchTag: '#materi_selector-search-input',
    contentTag: '#materi_selector-search-content',
    hideBy: 'opacity: 0; height: 0; border: none;',
  });
});

ev.subscribe('materi_selector:relayout', function relayout() {
  setTimeout(function () {
    var contentTag = document.getElementById('materi_selector-search-content');
    document.getElementById('materi_selector-search-notfound')
      .style.display = contentTag.scrollHeight < 40 ? 'block' : 'none';
  }, 400);
});

ev.subscribe('materi_selector:open', function open() {
  var materi_selector = document.getElementById('materi_selector');
  ev.publish('animate', {
    duration: 300,
    transition: function open(progress) {
      if (progress === 0) materi_selector.setAttribute('open', true);
      materi_selector.style.opacity = progress;
      if (progress === 1) {
        var content = document.getElementById('materi_selector-search-content').parentNode;
        if (!(content.clientHeight < content.scrollHeight)) {
          var searchInput = document.getElementById('materi_selector-search-input');
          while (!searchInput.classList.contains('modal-header-row')) {
            searchInput = searchInput.parentElement;
          }
          searchInput.style.display = 'none';
        }
      }
    },
  });
});

ev.subscribe('materi_selector:close', function close() {
  ev.publish('modal:close', { dialog: document.getElementById('materi_selector') });
});

ev.subscribe('materi_selector:toggle_audio', function toggle_audio(data) {
  data.li.classList.toggle('selected');
  if (data.li.classList.contains('selected')) {
    var input = document.createElement('input');
    input.type = 'hidden';
    input.name = 'audio_hashes[]';
    input.value = data.li.dataset.hash;
    data.li.appendChild(input);
  } else {
    data.li.removeChild(data.li.querySelector('input'));
  }
});

ev.subscribe('materi_selector:audio_upload', function upload() {
  var request = new XMLHttpRequest();
  request.open('POST', '/api/audio_upload.php', true);
  request.addEventListener('readystatechange', () => {
    if (request.readyState === XMLHttpRequest.DONE) {
      if (request.status === 200) {
        var audios = JSON.parse(request.responseText);
        var content = document.getElementById('materi_selector-search-content');
        var html = audios
          .reverse()
          .filter(function (audio) {
            return !audio.failed;
          })
          .reduce(function (html, audio) {
            const duplicate = content.querySelector('[data-value="' + audio.hash + '"]');
            if (duplicate) content.removeChild(duplicate.parentNode);
            return html + '<li class="list-view-item ellipsis selected" data-hash="' + audio.hash + '" style="max-width: unset;" onclick="ev.publish(\'materi_selector:toggle_library_audio\', { li: this });"><input type="hidden" name="audio_hashes[]" value="' + audio.hash + '"><div class="list-view-item-row"><span class="duration" style="font-size: .9rem; color: #424242; padding-left: .75rem;">' + audio.duration + '</span><div class="list-view-item-title"><span class="flex-ellipsis" style="color: #2E7D32;">' + audio.filename + '</span></div></div></li>';
          }, '');
        content.insertAdjacentHTML('afterbegin', html);
      } else alert(`MASALAH SERVER: ${request.responseText}`);
      ev.publish("materi_selector:audio_upload_done");
    }
  });
  request.addEventListener('error', () => {
    alert(`MASALAH KONEKSI`);
  });
  request.send(new FormData(document.getElementById('materi_selector_upload_form')));
});

ev.subscribe('materi_selector:audio_upload', function buttonControl() {
  var button = document.getElementById('audio_upload_button');
  var buttonText = document.getElementById('audio_upload_button_text');
  button.disabled = true;
  buttonText._textContent = buttonText.textContent;
  buttonText.textContent = 'Proses...';
});

ev.subscribe('materi_selector:audio_upload_done', function buttonControl() {
  var button = document.getElementById('audio_upload_button');
  var buttonText = document.getElementById('audio_upload_button_text');
  button.disabled = false;
  buttonText.textContent = buttonText._textContent;
});
