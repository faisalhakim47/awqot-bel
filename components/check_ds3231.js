http.request('GET', '/api/check_ds3231.php')
  .then(function (result) {
    if (result.is_error) {
      ds3231_error_notice_data.textContent = result.data;
      ds3231_error_notice_whatsapp.href = 'https://wa.me/6282147824783?text=' + encodeURIComponent(result.data);
      ev.publish('modal:open', { dialog: ds3231_error_notice });
    }
  });
