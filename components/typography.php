<?php

require_once __DIR__ . "/event.php";

subscribe("head", function () {
  echo "<style>";
  require_once __DIR__ . "/typography.css";
  echo "</style>";
});
