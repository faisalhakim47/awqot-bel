<?php

require_once __DIR__ . "/event.php";
require_once __DIR__ . "/../api/tools/awqot.php";

if (!is_scheduling_active()) {
  subscribe("head", function () {
    echo "<script>";
    require_once __DIR__ . "/check_scheduling.js";
    echo "</script>";
  });
  
  subscribe("body", function () {
    require_once __DIR__ . "/check_scheduling.html";
  });
}
