<?php

require_once __DIR__ . "/basic_style.php";
require_once __DIR__ . "/event.php";

subscribe("head", function () {
  echo "<style>";
  require_once __DIR__ . "/modal.css";
  echo "</style>";
});

subscribe("head", function () {
  echo "<script>";
  require_once __DIR__ . "/modal.js";
  echo "</script>";
});
