<?php

require_once __DIR__ . "/basic_style.php";
require_once __DIR__ . "/event.php";
require_once __DIR__ . "/list_view.php";
require_once __DIR__ . "/modal.php";
require_once __DIR__ . "/speaker.php";

subscribe("head", function () {
  echo "<script>";
  require_once __DIR__ . "/materi_player.js";
  echo "</script>";
});

subscribe("body", function () {
  require_once __DIR__ . "/materi_player.html";
});
