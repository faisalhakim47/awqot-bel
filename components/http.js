function request(method, url, options) {
    options = options || {};
    return new Promise(function (resolve, reject) {
        var request = new XMLHttpRequest();
        request.open(method, url, true);
        request.addEventListener('readystatechange', function () {
            if (request.readyState !== request.DONE) return;
            try {
                var result = JSON.parse(request.responseText);
            } catch (error) {
                var result = request.responseText;
            }
            if (request.status === 200) {
                resolve(result);
            } else {
                reject({
                    request: request,
                    result: result,
                });
            }
        });
        request.addEventListener('error', function (error) {
            reject({
                request: request,
                error: error,
            });
        });
        request.send(options.data);
    });
}

return {
    request,
};
