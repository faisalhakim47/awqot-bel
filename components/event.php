<?php

$__subscribers = [];

function subscribe($name, $subscriber) {
  global $__subscribers;
  if (!isset($__subscribers[$name])) $__subscribers[$name] = [];
  array_push($__subscribers[$name], $subscriber);
}

function publish($name, $data = []) {
  global $__subscribers;
  if (isset($__subscribers[$name])) {
    foreach ($__subscribers[$name] as $subscriber) {
      $subscriber($data);
    }
  }
}

subscribe("head", function () {
  echo "<script>";
  require_once __DIR__ . "/event.js";
  echo "</script>";
});
