<?php

require_once __DIR__ . "/animation.php";
require_once __DIR__ . "/basic_style.php";
require_once __DIR__ . "/event.php";

subscribe("head", function () {
  echo "<style>";
  require_once __DIR__ . "/sidebar.css";
  echo "</style>";
});

subscribe("head", function () {
  echo "<script>";
  require_once __DIR__ . "/sidebar.js";
  echo "</script>";
});

subscribe("body", function () {
  $sidebar_list = [
    [
      "name" => "Beranda",
      "href" => "/",
      "icon" => __DIR__ . "/../static/icons/round-dashboard-24px.svg",
    ],
    [
      "separator" => true,
      "name" => "Penjadwalan",
    ],
    [
      "name" => "Paket Materi",
      "href" => "/pages/schedule-list.php",
      "icon" => __DIR__ . "/../static/icons/round-event-24px.svg",
    ],
    [
      "name" => "Perubahan Paket",
      "href" => "/pages/schedule_change-list.php",
      "icon" => __DIR__ . "/../static/icons/round-date_range-24px.svg",
    ],
    [
      "separator" => true,
      "name" => "Materi",
    ],
    [
      "name" => "Materi Player",
      "href" => "/pages/playlist-list.php",
      "icon" => __DIR__ . "/../static/icons/round-play_circle_filled-24px.svg",
    ],
    [
      "name" => "Pengelola Materi",
      "href" => "/pages/materi-manager.php",
      "icon" => __DIR__ . "/../static/icons/round-library_music-24px.svg",
    ],
    [
      "separator" => true,
      "name" => "Pengaturan",
    ],
    [
      "name" => "Speaker",
      "href" => "/pages/speaker.php",
      "icon" => __DIR__ . "/../static/icons/round-speaker-24px.svg",
    ],
    [
      "name" => "Wifi dan Password",
      "href" => "/pages/wifi.php",
      "icon" => __DIR__ . "/../static/icons/round-wifi-24px.svg",
    ],
    [
      "name" => "Sistem",
      "href" => "/pages/about.php",
      "icon" => __DIR__ . "/../static/icons/round-system_update-24px.svg",
    ],
  ];

  ?>
<aside id="sidebar" class="sidebar">
  <div class="sidebar-overlay" style="display: none; opacity: 0;" onclick="ev.publish('sidebar:close');"></div>
  <ul class="sidebar-list">
    <?php foreach ($sidebar_list as $sidebar_item): ?>
    <?php if (!empty($sidebar_item["separator"])): ?>
    <li class="sidebar-separator"><?= $sidebar_item["name"] ?></li>
    <?php else: ?>
    <li class="sidebar-item <?= $sidebar_item["href"] === $_SERVER["REQUEST_URI"] ? "active" : "" ?>">
      <a href="<?= $sidebar_item["href"] ?>">
        <span class="icon">
          <?php include $sidebar_item["icon"]; ?>
        </span>
        <span class="text"><?= $sidebar_item["name"] ?></span>
      </a>
    </li>
    <?php endif; ?>
    <?php endforeach; ?>
  </ul>
</aside>
  <?php
});
