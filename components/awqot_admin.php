<?php

require_once __DIR__ . "/../api/tools/database.php";
require_once __DIR__ . "/../api/tools/is_online.php";

require_once __DIR__ . "/event.php";
require_once __DIR__ . "/http.php";

subscribe("head", function () {
  $awqot_admin_data = [
    "rpiSerialNumber" => exec("cat /proc/cpuinfo | grep Serial | awk ' {print $3}'"),
    "productKey" => get_configuration("product_key"),
  ];
  ?><script
    id="awqot_admin_data"
    type="application/json"
  ><?= json_encode($awqot_admin_data) ?></script><?php
});

if (is_online()) {
  subscribe("body", function () {
    echo "<script>window.awqotAdmin = (function () {";
    require_once __DIR__ . "/awqot_admin.js";
    echo "})()</script>";
  });

  subscribe("body", function () {
    require_once __DIR__ . "/awqot_admin.html";
  });
}