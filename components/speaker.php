<?php

require_once __DIR__ . "/event.php";
require_once __DIR__ . "/../api/tools/configuration.php";

subscribe("head", function () {
  ?><script>window.speakerPins = JSON.parse('<?= get_configuration("speaker_pins") ?>')</script><?php
});
