<?php

require_once __DIR__ . "/api/commands/awqot_ensure_upgrade.php";
require_once __DIR__ . "/api/tools/array.php";
require_once __DIR__ . "/api/tools/awqot.php";
require_once __DIR__ . "/api/tools/configuration.php";
require_once __DIR__ . "/api/tools/database.php";
require_once __DIR__ . "/api/tools/is_online.php";
require_once __DIR__ . "/api/tools/time.php";

// ---------- MODEL ----------

$is_institution_photo_exist = file_exists(__DIR__ . "/data/institution_photo");

$awqot_institution_name = get_configuration("awqot_institution_name");
$awqot_institution_address = get_configuration("awqot_institution_address");

$now = round(microtime(true) * 1000);
$scheduled_times = json_decode(file_get_contents(__DIR__ . "/data/scheduled_times.json"), true);

$scheduled_times = array_map(function ($scheduled_time) use ($now) {
  $audio_end_time = $scheduled_time["audio_begin_time"] + $scheduled_time["total_duration"];
  return [
    "is_past" => $scheduled_time["audio_begin_time"] < $now,
    "is_present" => $scheduled_time["audio_begin_time"] < $now && $audio_end_time > $now,
    "is_future" => $scheduled_time["audio_begin_time"] > $now,
    "begin_time" => humanize_time($scheduled_time["audio_begin_time"]),
    "end_time" => humanize_time($audio_end_time),
    "audio_begin_time" => $scheduled_time["audio_begin_time"],
    "audios" => $scheduled_time["audios"],
    "total_duration" => $scheduled_time["total_duration"],
  ];
}, $scheduled_times);

$scheduled_time_next = array_find($scheduled_times, function ($scheduled_time) use ($now) {
  return $scheduled_time["audio_begin_time"] > $now;
}) ?: $scheduled_times[0];

require_once __DIR__ . "/components/awqot_admin.php";
require_once __DIR__ . "/components/awqot_time.php";
require_once __DIR__ . "/components/basic_style.php";
require_once __DIR__ . "/components/box.php";
require_once __DIR__ . "/components/button.php";
require_once __DIR__ . "/components/button_group.php";
require_once __DIR__ . "/components/check_ds3231.php";
require_once __DIR__ . "/components/check_scheduling.php";
require_once __DIR__ . "/components/head.php";
require_once __DIR__ . "/components/icon.php";
require_once __DIR__ . "/components/notification.php";
require_once __DIR__ . "/components/page.php";
require_once __DIR__ . "/components/register_notice.php";
require_once __DIR__ . "/components/sidebar.php";
require_once __DIR__ . "/components/table.php";

?><!DOCTYPE html>

<html lang="id">

<head>
  <title>Beranda - Awqot</title>
  <?php publish("head"); ?>
</head>

<body>
  <?php publish("body"); ?>
  <main class="page" style="padding-top: 3.5rem;">
    <header class="page-header">
      <div class="page-header-row">
        <div class="page-header-left">
          <button type="button" class="button" onclick="ev.publish('sidebar:open');">
            <span class="icon">
              <?php include __DIR__ . "/static/icons/round-menu-24px.svg" ?>
            </span>
          </button>
          <h1 class="page-title">Beranda</h1>
        </div>
      </div>
    </header>
    <div class="page-content">
      <?php if ($is_institution_photo_exist): ?>
      <div
        style="
          background-image: url('/data/institution_photo');
          background-position: center;
          background-size: contain;
          background-repeat: no-repeat;
          height: 250px;
          display: flex;
          justify-content: center;
          align-items: flex-end;
          margin: 1rem;
        "
      ></div>
      <p
        style="
          box-sizing: border-box;
          width: 100%;
          margin: 0 auto;
          padding: 1rem;
          background-color: rgba(255, 255, 255, .8);
          font-size: 1.5rem;
          text-align: center;
          margin-bottom: 1rem;
        "
      ><?= $awqot_institution_name ?><br><span style="display: block; font-size: .9rem; font-weight: 400; line-height: 1.25;"><?= $awqot_institution_address ?></span></p>
      <?php else: ?>
      <p>
        <img
          src="/static/images/save-time.png"
          alt="Logo"
          style="
            display: block;
            width: 12rem;
            margin: 2rem auto;
          "
        >
      </p>
      <?php endif; ?>
      <p
        style="
          margin: 0 auto .75rem auto;
          font-size: 1.3rem;
          font-weight: 300;
          text-align: center;
        "
      >Jadwal selanjutnya:</p>
      <p
        id="countdown-time"
        data-time="<?= $scheduled_time_next["audio_begin_time"] ?>"
        style="
          margin: 0 auto;
          font-size: 2.5rem;
          font-weight: 300;
          font-family: monospace;
          text-align: center;
        "
      >--:--:--</p>
      <p
        style="
          padding: 0 2rem;
          font-size: 1.25rem;
          font-weight: 300;
          line-height: 1.5;
          text-align: center;
        "
      >waktu di alat Awqot <span id="awqot-time" data-time="<?= $now ?>" style="font-family: monospace;font-size: .8em;">--:--:--</span> <span id="awqot_time_correct" class="icon small positive"><?php include __DIR__ . "/static/icons/round-check_circle_outline-24px.svg" ?></span></p>

      <div style="padding: 0 1rem; box-sizing: border-box;">
      </div>

    </div>
  </main>

  <script>
  var now = deviceTime;
  var countdownTimeEl = document.getElementById("countdown-time");
  var countdownTimeText = countdownTimeEl.childNodes[0];
  var countdownTime = parseInt(countdownTimeEl.dataset.time, 10);
  var awqotTimeEl = document.getElementById("awqot-time");
  var awqotTimeText = awqotTimeEl.childNodes[0];
  var awqotTime = awqotTime;
  var awqotTimeDiff = awqotTimeDiff;

  if (awqotTimeDiff < -5000 || awqotTimeDiff > 5000) {
    awqot_time_correct.style.display = 'none';
  }

  function toTwoDigit(number) {
    return number < 10 ? '0' + number.toString() : number;
  }

  function humanizeTime(date) {
    return toTwoDigit(date.getHours()) + ':' + toTwoDigit(date.getMinutes()) + ':' + toTwoDigit(date.getSeconds());
  }

  function refreshTime() {
    var date = new Date();
    var now = date.getTime();
    date.setMilliseconds(0);
    date.setSeconds(0);
    date.setMinutes(0);
    date.setHours(0);
    var awqot = now + awqotTimeDiff;
    var countdown = date.getTime() + (countdownTime - awqot);
    requestAnimationFrame(function (timePass) {
      timePass = timePass / 1000;
      if (!isNaN(countdownTime)) countdownTimeText.nodeValue = humanizeTime(new Date(countdown + timePass));
      awqotTimeText.nodeValue = humanizeTime(new Date(awqot + timePass));
    });
  }

  refreshTime();
  setInterval(refreshTime, 1000);
  </script>
</body>

</html>
