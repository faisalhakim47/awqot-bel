<?php

require_once __DIR__ . "/../tools/upgrade.php";

alter_structure(function () {

  execute_sql("
    ALTER TABLE schedule_times ADD relay_pins VARCHAR(191) AFTER audios
  ");

  execute_sql("
    UPDATE schedule_times SET relay_pins = '[18,17,27]'
  ");

});
