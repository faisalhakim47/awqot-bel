<?php

require_once __DIR__ . "/../tools/upgrade.php";

alter_structure(function () {
  $bashrc = '/home/pi/.bashrc';
  file_put_contents(
    $bashrc,
    str_replace(
      "php-cgi /home/pi/awqot-bel/api/commands/init.php",
      "",
      file_get_contents($bashrc)
    )
  );
  $bash_profile = '/home/pi/.bash_profile';
  file_put_contents(
    $bash_profile,
    file_get_contents($bash_profile) . "
php-cgi /home/pi/awqot-bel/api/commands/init.php
"
  );
});
