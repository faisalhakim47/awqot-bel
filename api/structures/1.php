<?php

require_once __DIR__ . "/../tools/upgrade.php";

alter_structure(function () {

  create_table("schedules", "
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    name VARCHAR(191) NOT NULL,
    PRIMARY KEY(id)
  ");

  create_table("schedule_times", "
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    schedule_id INT UNSIGNED NOT NULL,
    day TINYINT UNSIGNED NOT NULL,
    hour TINYINT UNSIGNED NOT NULL,
    minute TINYINT UNSIGNED NOT NULL,
    audios TEXT NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(schedule_id) REFERENCES schedules(id)
  ");

  create_table("audios", "
    hash VARCHAR(191) NOT NULL,
    filename VARCHAR(191) NOT NULL,
    filetype VARCHAR(191) NOT NULL,
    duration INT UNSIGNED NOT NULL,
    PRIMARY KEY(hash)
  ");

  create_table("playlists", "
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    name VARCHAR(191) NOT NULL,
    audios TEXT NOT NULL,
    PRIMARY KEY(id)
  ");

  execute_insert_sql("configurations", [
    "name" => ["active_schedule_id", PDO::PARAM_STR],
    "value" => ["1", PDO::PARAM_STR],
  ]);

});
