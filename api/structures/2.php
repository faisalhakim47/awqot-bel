<?php

require_once __DIR__ . "/../tools/upgrade.php";

alter_structure(function () {

  create_table("schedule_changes", "
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    date TINYINT UNSIGNED NOT NULL,
    month TINYINT UNSIGNED NOT NULL,
    calendar VARCHAR(191) NOT NULL,
    schedule_id INT UNSIGNED NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(schedule_id) REFERENCES schedules(id)
  ");

  execute_insert_sql("configurations", [
    "name" => ["speaker_pins", PDO::PARAM_STR],
    "value" => ["[17, 18, 22, 27]", PDO::PARAM_STR],
  ]);

  execute_insert_sql("configurations", [
    "name" => ["awqot_id", PDO::PARAM_STR],
    "value" => ["", PDO::PARAM_STR],
  ]);

  execute_insert_sql("configurations", [
    "name" => ["awqot_password", PDO::PARAM_STR],
    "value" => ["", PDO::PARAM_STR],
  ]);

  execute_insert_sql("configurations", [
    "name" => ["awqot_customer_name", PDO::PARAM_STR],
    "value" => ["", PDO::PARAM_STR],
  ]);

  execute_insert_sql("configurations", [
    "name" => ["awqot_customer_phonenumber", PDO::PARAM_STR],
    "value" => ["", PDO::PARAM_STR],
  ]);

  execute_insert_sql("configurations", [
    "name" => ["awqot_institution_name", PDO::PARAM_STR],
    "value" => ["", PDO::PARAM_STR],
  ]);

  execute_insert_sql("configurations", [
    "name" => ["awqot_institution_address", PDO::PARAM_STR],
    "value" => ["", PDO::PARAM_STR],
  ]);

});
