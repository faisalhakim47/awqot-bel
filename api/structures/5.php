<?php

require_once __DIR__ . "/../tools/child_process.php";
require_once __DIR__ . "/../tools/configuration.php";
require_once __DIR__ . "/../tools/upgrade.php";

alter_structure(function () {
  set_configuration("product_key", get_configuration("awqot_id") ?: "");
  async_exec("php-cgi /home/pi/awqot-bel/api/commands/init.php");
  $bashrc_path = "/home/pi/.bashrc";
  $old_bashrc = file_get_contents($bashrc_path);
  $new_bashrc = str_replace(
    "php-cgi /home/pi/awqot-bel/api/scheduling_compute.php",
    "php-cgi /home/pi/awqot-bel/api/commands/init.php",
    $old_bashrc
  );
  file_put_contents($bashrc_path, $new_bashrc);
});
