<?php
require_once __DIR__ . "/../tools/upgrade.php";

alter_structure(function () {
  shell_exec("sudo rm -rf /etc/apache2/sites-enabled/awqot-masjid.conf");
  shell_exec("sudo rm -rf /etc/apache2/sites-enabled/awqot-bel.conf");
  shell_exec("sudo ln -s /etc/apache2/sites-available/awqot-bel.conf /etc/apache2/sites-enabled/awqot.conf");
});
