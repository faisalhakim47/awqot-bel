<?php

require_once __DIR__ . "/commands/awqot_ensure_upgrade.php";
require_once __DIR__ . "/tools/server.php";
require_once __DIR__ . "/tools/child_process.php";
require_once __DIR__ . "/tools/mediainfo.php";

$seek = require_querystring("seconds");

$materi_player_path = __DIR__ . "/../data/materi_player";
$playing = explode(" ", file_get_contents($materi_player_path));

if (strpos($playing[3], "__PLAYING__") === 0) {
  ob_start();
  require_once __DIR__ . "/audio_stop.php";
  ob_clean();
  $audio_path = __DIR__ . "/../data/audios/" . $playing[0];
  async_exec("cvlc --play-and-exit --start-time={$seek} {$audio_path}");
  $time_start = round(microtime(true) * 1000) - ($seek * 1000);
  $materi_player = "{$playing[0]} {$playing[1]} {$time_start} __PLAYING__";
  exec("touch '{$materi_player_path}' && echo '{$materi_player}' > '{$materi_player_path}'");
  echo $materi_player;
}
