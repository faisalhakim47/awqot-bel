<!DOCTYPE html>

<?php

require_once __DIR__ . "/tools/child_process.php";
require_once __DIR__ . "/tools/is_online.php";

if (is_online()) {
  $git_pull = shell_exec("cd " . realpath(__DIR__ . "/../") . " && git checkout . && git pull");
  async_exec("sleep 1 && sudo systemctl restart apache2");
  echo "<h1>Selesai</h1><p>Aplikasi berhasil diupgrade.</p>";
  echo "<pre><code>";
  echo $git_pull;
  echo "</code></pre>";
} else {
  echo "<h1>Gagal</h1><p>Maaf, anda tidak memiliki jaringan internet. Silahkan akses alat awqot melalui kabel USB dan coba update kembali. Pastikan smartphone anda terhubung dengan internet.</p>";
}

echo "<p>Mohon tunggu 10 detik, anda akan diarahkan kembali ke halaman utama.</p>";
echo "<script>setTimeout(function () {
  var request = new XMLHttpRequest();
  request.open('GET', '/api/commands/scheduling_compute.php');
  request.addEventListener('readystatechange', function () {
    if (request.readyState === request.DONE) location.pathname = '/';
  });
  request.send();
}, 5000);</script>";
