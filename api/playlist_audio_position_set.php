<?php

require_once __DIR__ . "/commands/awqot_ensure_upgrade.php";
require_once __DIR__ . "/tools/database.php";

$playlist_id = $_POST["playlist_id"];
$direction = $_POST["direction"];
$target_audio_index = $_POST["audio_index"];
$target_audio_hash = $_POST["audio_hash"];

$audios = execute_sql("
  SELECT playlists.audios AS audios
  FROM playlists
  WHERE playlists.id = :playlist_id
", [
  ":playlist_id" => [$playlist_id, PDO::PARAM_INT],
])->fetch();

$audios = json_decode($audios["audios"], true);

array_splice($audios, $target_audio_index, 1);

$new_index = $target_audio_index + ($direction === "up" ? -1 : 1);
$new_index = $new_index === -1 ? 0 : $new_index;

array_splice($audios, $new_index, 0, $target_audio_hash);

execute_update_sql("playlists", [
  "audios" => [json_encode($audios), PDO::PARAM_STR],
], [
  "id" => [$playlist_id, PDO::PARAM_INT],
]);
