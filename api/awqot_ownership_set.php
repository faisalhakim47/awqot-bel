<?php

require_once __DIR__ . "/tools/configuration.php";

set_configuration("awqot_customer_name", $_POST["name"]);
set_configuration("awqot_customer_phonenumber", $_POST["phoneNumber"]);
set_configuration("awqot_institution_name", $_POST["organizationName"]);
set_configuration("awqot_institution_address", $_POST["address"]);

$institution_photo_file = $_FILES["photo"]["tmp_name"];
$image_props = getimagesize($institution_photo_file);
$image_type = $image_props[2];
$institution_photo_filename = time() . image_type_to_extension($image_type);
move_uploaded_file($institution_photo_file, __DIR__ . "/../data/" . $institution_photo_filename);
set_configuration("institution_photo", $institution_photo_filename);
