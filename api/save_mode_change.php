<?php

$bash_profile = "/home/pi/.bash_profile";

if (isset($_POST["save_mode"]) && $_POST["save_mode"] === "active") {
  file_put_contents(
    $bash_profile,
    str_replace(
      "php-cgi /home/pi/awqot-bel/api/commands/init.php",
      "# php-cgi /home/pi/awqot-bel/api/commands/init.php",
      file_get_contents($bash_profile)
    )
  );
} else {
  file_put_contents(
    $bash_profile,
    str_replace(
      "# php-cgi /home/pi/awqot-bel/api/commands/init.php",
      "php-cgi /home/pi/awqot-bel/api/commands/init.php",
      file_get_contents($bash_profile)
    )
  );
}
