<?php

require_once __DIR__ . "/commands/awqot_ensure_upgrade.php";
require_once __DIR__ . "/tools/database.php";
require_once __DIR__ . "/tools/mediainfo.php";
require_once __DIR__ . "/tools/time.php";

$audios = [];

if (isset($_FILES["audio_files"])) {
  $audio_files_length = count($_FILES["audio_files"]["name"]);
  for ($index = 0; $index < $audio_files_length; $index++) {
    $temporary_file = $_FILES["audio_files"]["tmp_name"][$index];
    $filename = $_FILES["audio_files"]["name"][$index];
    if ($temporary_file === "") {
      array_push($audios, [
        "failed" => true,
        "temporary_file" => $temporary_file,
        "filename" => $filename,
        "debug" => $_FILES["audio_files"],
      ]);
      continue;
    }
    $hash = md5_file($temporary_file);
    $audio = execute_sql("
      SELECT
        audios.hash AS hash,
        audios.filename AS filename,
        audios.filetype AS filetype,
        audios.duration AS duration
      FROM audios
      WHERE audios.hash = :hash
    ", [
      ":hash" => [$hash, PDO::PARAM_STR],
    ])->fetch();
    if (!$audio) {
      $new_file = __DIR__ . "/../data/audios/" . $hash;
      move_uploaded_file($temporary_file, $new_file);
      $filemeta = mediainfo($new_file);
      $audio = [
        "hash" => $hash,
        "filename" => $filename,
        "filetype" => $filemeta["Internet media type"],
        "duration" => $filemeta["Duration"],
      ];
      execute_insert_sql("audios", [
        "hash" => [$audio["hash"], PDO::PARAM_STR],
        "filename" => [$audio["filename"], PDO::PARAM_STR],
        "filetype" => [$audio["filetype"], PDO::PARAM_STR],
        "duration" => [$audio["duration"], PDO::PARAM_INT],
      ]);
    }
    array_push($audios, $audio);
  }
}

$audios = array_map(function ($audio) {
  $audio["duration"] = humanize_duration($audio["duration"]);
  return $audio;
}, $audios);

send_json(200, $audios);
