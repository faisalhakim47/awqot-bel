<?php

require_once __DIR__ . "/../commands/awqot_ensure_upgrade.php";
require_once __DIR__ . "/../tools/child_process.php";
require_once __DIR__ . "/../tools/configuration.php";
require_once __DIR__ . "/../tools/database.php";
require_once __DIR__ . "/../tools/raspberry.php";

(function () {
  $app_dir = realpath(__DIR__ . "/../..");

  // ---------- NETRALIZE ----------

  require __DIR__ . "/scheduling_stop.php";
  async_exec("php-cgi {$app_dir}/api/commands/speaker_off.php");
  exec("sudo hwclock -s");

  // ---------- SCHEDULE CHANGER ----------

  require_once __DIR__ . "/schedule_changer.php";

  // ---------- MODEL ----------

  $active_schedule_id = get_configuration("active_schedule_id");

  if (!$active_schedule_id) {
    return;
  }

  $time = round(microtime(true) * 1000);
  $day = (int) date("w");

  $schedule_times = execute_sql("
  SELECT
    schedule_times.hour AS hour,
    schedule_times.minute AS minute,
    schedule_times.audios AS audios,
    schedule_times.relay_pins AS relay_pins
  FROM schedule_times
  WHERE schedule_times.schedule_id = :schedule_id
    AND schedule_times.day = :day
", [
    ":schedule_id" => [$active_schedule_id, PDO::PARAM_INT],
    ":day" => [$day, PDO::PARAM_INT],
  ])->fetchAll();

  $scheduled_times = [];

  foreach ($schedule_times as $schedule_time) {
    $audio_hashes = json_decode($schedule_time["audios"], true);

    $audios = array_map(function ($audio_hash) {
      return execute_sql("
        SELECT
          audios.hash AS hash,
          audios.duration AS duration
        FROM audios
        WHERE audios.hash = :audio_hash
      ", [
        ":audio_hash" => [$audio_hash, PDO::PARAM_STR],
      ])->fetch();
    }, $audio_hashes);

    $total_duration = 0;

    foreach ($audios as $audio) {
      $total_duration += $audio["duration"];
    }

    $audio_begin_time = mktime(
      $schedule_time["hour"],
      $schedule_time["minute"],
      0
    ) * 1000;
    $speaker_begin_time = $audio_begin_time - 5000;
    $audio_end_time = $audio_begin_time + $total_duration;
    $speaker_end_time = $audio_end_time + 2500;

    // NO CONTENT
    if ($total_duration === 0) {
      continue;
    }

    array_push($scheduled_times, [
      "audios" => $audio_hashes,
      "audio_begin_time" => $audio_begin_time,
      "total_duration" => $total_duration,
    ]);

    $switch_speaker = function ($sleep_time, $condition) use ($schedule_time) {
      $relay_pins = json_decode($schedule_time["relay_pins"], false);
      $shell_commands = array_map(function ($pin) use ($condition) {
        return switch_relay_sh($pin, $condition);
      }, $relay_pins);
      shuffle($shell_commands);
      $shell_command = implode(" && ", $shell_commands);
      if ($shell_command) {
        async_exec("sleep {$sleep_time} && {$shell_command}");
      }
    };

    // PAST
    if ($audio_end_time <= $time) {
      continue;
    }

    // FUTURE
    else if ($audio_begin_time > $time) {
      $audio_begin_countdown = ($audio_begin_time - $time) / 1000;
      $audio_paths = implode(" ", array_map(function ($audio_hash) use ($app_dir) {
        return "{$app_dir}/data/audios/{$audio_hash}";
      }, $audio_hashes));
      $speaker_begin_countdown = ($speaker_begin_time - $time) / 1000;
      $speaker_begin_countdown = $speaker_begin_countdown < 0 ? 0 : $speaker_begin_countdown;

      $switch_speaker($speaker_begin_countdown, "on");
      async_exec("sleep {$audio_begin_countdown} && mplayer {$audio_paths}");
    }

    // PRESENT
    else if ($audio_begin_time <= $time && $audio_end_time > $time) {
      $audio_skipped_time = $time - $audio_begin_time;
      $audio_paths = array_map(function ($audio_hash) use ($app_dir) {
        return "{$app_dir}/data/audios/{$audio_hash}";
      }, $audio_hashes);

      $first_audio = "";
      $next_audios = [];
      $next_audios_delay = 0;
      foreach ($audio_paths as $audio_path) {
        if ($first_audio === "") {
          $duration = (int) exec("mediainfo --Inform='Audio;%Duration%' '{$audio_path}'");
          if ($duration < $audio_skipped_time) {
            $audio_skipped_time = $audio_skipped_time - $duration;
            continue;
          }
          $first_audio = $audio_path;
          $next_audios_delay = $duration - $audio_skipped_time;
        } else {
          array_push($next_audios, $audio_path);
        }
      }
      $audio_skipped_time = $audio_skipped_time / 1000;
      $next_audios_delay = $next_audios_delay / 1000;
      $next_audios_string = implode(" ", $next_audios);
      $switch_speaker(0, "on");
      async_exec("cvlc --play-and-exit --start-time={$audio_skipped_time} {$first_audio}");
      if (count($next_audios) !== 0) {
        async_exec("sleep {$next_audios_delay} && mplayer {$next_audios_string}");
      }
    }

    $speaker_end_countdown = ($speaker_end_time - $time) / 1000;
    $switch_speaker($speaker_end_countdown, "off");
  }

  $recompute_countdown = mktime(1, 1, 1, date("n"), date("j") + 1) - time();
  async_exec("sleep {$recompute_countdown} && php-cgi {$app_dir}/api/commands/scheduling_compute.php");

  file_put_contents($app_dir . "/data/scheduled_times.json", json_encode($scheduled_times));
})();
