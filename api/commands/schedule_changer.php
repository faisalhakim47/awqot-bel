<?php

require_once __DIR__ . "/../tools/database.php";
require_once __DIR__ . "/../tools/configuration.php";

$today_hijriah = explode("/", explode(" ", shell_exec("idate -s"))[1]);
$today_hijriah["date"] = (int) $today_hijriah[0];
$today_hijriah["month"] = (int) $today_hijriah[1];
$today_masehi = [
  "date" => (int) date("j"),
  "month" => (int) date("n"),
];

$schedule_changes = [
  execute_sql("
    SELECT *
    FROM schedule_changes
    WHERE `calendar` = 'Hijriah'
      AND `date` = :date
      AND `month` = :month
  ", [
    ":date" => [$today_hijriah["date"], PDO::PARAM_INT],
    ":month" => [$today_hijriah["month"], PDO::PARAM_INT],
  ])->fetch(),
  execute_sql("
    SELECT *
    FROM schedule_changes
    WHERE `calendar` = 'Masehi'
      AND `date` = :date
      AND `month` = :month
  ", [
    ":date" => [$today_masehi["date"], PDO::PARAM_INT],
    ":month" => [$today_masehi["month"], PDO::PARAM_INT],
  ])->fetch(),
];

if ($schedule_changes[0]) $schedule_change = $schedule_changes[0];
if ($schedule_changes[1]) $schedule_change = $schedule_changes[1];
if ($schedule_changes[0] && $schedule_changes[1]) {
  $schedule_change = $schedule_changes[0]["id"] < $schedule_changes[1]["id"]
    ? $schedule_changes[0] : $schedule_changes[1];
}

if (isset($schedule_change) && $schedule_change) {
  set_configuration("active_schedule_id", $schedule_change["schedule_id"]);
}
