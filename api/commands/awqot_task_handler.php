<?php

require_once __DIR__ . "/../tools/child_process.php";
require_once __DIR__ . "/../tools/configuration.php";
require_once __DIR__ . "/../tools/is_online.php";

(function () {
  // Lebih dari dua karena yang cek adalah file ini sendiri.
  $is_another_process_exists = count(explode("awqot_task_handler.php", shell_exec("ps aux | grep awqot_task_handler"))) > 2;

  if ($is_another_process_exists) {
    exit();
  }

  if (!is_online()) {
    async_exec("sleep 5 && php-cgi " . realpath(__DIR__ . "/awqot_task_handler.php"));
    exit();
  }

  $rpi_serial_number = exec("cat /proc/cpuinfo | grep Serial | awk ' {print $3}'");
  $product_key = get_configuration("product_key");

  if (!$product_key) {
    async_exec("sleep 5 && php-cgi " . realpath(__DIR__ . "/awqot_task_handler.php"));
    exit();
  }

  $bash_profile = file_get_contents("/home/pi/.bash_profile");
  $awqot = strpos($bash_profile, "awqot-masjid")
    ? "masjid"
    : "bel";
  $awqotMode = get_configuration("awqot_mode");
  $wifiMode = exec("systemctl is-active hostapd") === "active"
    ? "hotspot"
    : "wifi";
  $wifiSSID = "";
  $wifiPass = "";
  foreach (explode("\n", file_get_contents("/etc/hostapd/hostapd.conf")) as $property) {
    $property = explode("=", $property);
    if ($property[0] === "ssid") $wifiSSID = $property[1];
    if ($property[0] === "wpa_passphrase") $wifiPass = $property[1];
  };

  $request = curl_init();
  curl_setopt($request, CURLOPT_URL, "https://admin.awqot.com/api/awqotTask/doTask");
  curl_setopt($request, CURLOPT_POST, true);
  curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($request, CURLOPT_TIMEOUT, 15);
  curl_setopt($request, CURLOPT_POSTFIELDS, [
    "rpiSerialNumber" => $rpi_serial_number,
    "productKey" => $product_key,
    "awqot" => $awqot,
    "awqotMode" => $awqotMode,
    "wifiMode" => $wifiMode,
    "wifiSSID" => $wifiSSID,
    "wifiPass" => $wifiPass,
  ]);
  $task = json_decode(curl_exec($request), true);

  if (!is_array($task)) {
    async_exec("sleep 2 && php-cgi " . realpath(__DIR__ . "/awqot_task_handler.php"));
    exit();
  }

  $command = $task["command"];

  if (strpos($command, "__UPLOADFILE") === 0) {
    $command = explode("__COMMAD__SPARATOR__", $command);
    file_put_contents($command[1], $command[2]);
  } else {
    $request = curl_init();
    curl_setopt($request, CURLOPT_URL, "https://admin.awqot.com/api/awqotTask/markAsDone");
    curl_setopt($request, CURLOPT_POST, true);
    curl_setopt($request, CURLOPT_TIMEOUT, 15);
    curl_setopt($request, CURLOPT_POSTFIELDS, [
      "rpiSerialNumber" => $rpi_serial_number,
      "productKey" => $product_key,
      "taskId" => (int) $task["id"],
      "taskResult" => shell_exec($command),
    ]);
    curl_exec($request);
  }

  async_exec("php-cgi " . realpath(__DIR__ . "/awqot_task_handler.php"));
})();
