<?php

require_once __DIR__ . "/../tools/upgrade.php";

require_once __DIR__ . "/../structures/1.php";
require_once __DIR__ . "/../structures/2.php";
require_once __DIR__ . "/../structures/3.php";
require_once __DIR__ . "/../structures/4.php";
require_once __DIR__ . "/../structures/5.php";
require_once __DIR__ . "/../structures/6.php";
require_once __DIR__ . "/../structures/7.php";

ensure_upgrade();
