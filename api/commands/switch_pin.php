<?php

require_once __DIR__ . "/../tools/raspberry.php";

if (empty($_GET["pin"]) || empty($_GET["condition"])) {
  send_json(400, [
    "ok" => false,
  ]);
}

switch_relay((int) $_GET["pin"], $_GET["condition"]);
