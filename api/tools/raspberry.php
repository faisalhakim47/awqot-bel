<?php

require_once __DIR__ . "/database.php";
require_once __DIR__ . "/configuration.php";

function pin_switch($pin, $condition) {
  exec("gpio -g mode {$pin} out && gpio -g write {$pin} {$condition}");
}

function speaker_on() {
  $speaker_pins = json_decode(get_configuration("speaker_pins"), false);
  shuffle($speaker_pins);
  $awqot_mode = get_configuration("awqot_mode");
  if ($awqot_mode === "default") {
    $condition = 0;
  } else if ($awqot_mode === "mini") {
    $condition = 1;
  } else {
    throw new Error("awqot_mode not set");
  }
  foreach ($speaker_pins as $pin) {
    pin_switch($pin, $condition);
  }
}

function speaker_off() {
  $speaker_pins = json_decode(get_configuration("speaker_pins"), false);
  shuffle($speaker_pins);
  $awqot_mode = get_configuration("awqot_mode");
  if ($awqot_mode === "default") {
    $condition = 1;
  } else if ($awqot_mode === "mini") {
    $condition = 0;
  } else {
    throw new Error("awqot_mode not set");
  }
  foreach ($speaker_pins as $pin) {
    pin_switch($pin, $condition);
  }
}

function pin_switch_sh($pin, $condition) {
  return "gpio -g mode {$pin} out && gpio -g write {$pin} {$condition}";
}

function switch_relay_sh($pin, $condition) {
  return pin_switch_sh($pin, $condition === 'on' ? 0 : 1);
}

function switch_relay($pin, $condition) {
  return pin_switch($pin, $condition === 'on' ? 0 : 1);
}
