<?php

$config_file = __DIR__ . "/../../config.php";

if (file_exists($config_file)) {
  require_once $config_file;
}
else {
  define("DB_NAME", "awqot_bel");
  define("DB_USER", "root");
  define("DB_PASS", "root");
}

require_once __DIR__ . "/server.php";

$pdo = (function () {
  $DB_HOST = defined("DB_HOST") ? DB_HOST : "127.0.0.1";
  $dsn = "mysql:host=" . $DB_HOST . ";dbname=" . DB_NAME;
  return new PDO($dsn, DB_USER, DB_PASS, [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES => false,
  ]); 
})();

$sql_transaction_dept = 0;

/**
 * It can execute only one query per call for security reason.
 */
function execute_sql($sql_query, $params = [])
{
  global $pdo;
  global $sql_transaction_dept;
  try {
    $statement = $pdo->prepare($sql_query);
  } catch (PDOException $error) {
    if ($sql_transaction_dept !== 0) {
      $pdo->rollBack();
    }
    send_json(500, [
      "sql_query" => $sql_query,
      "error" => $error,
    ]);
    throw $error;
  }
  foreach ($params as $key => $param) {
    if (!is_array($param)) {
      $param = [$param];
    }

    if (is_numeric($key)) {
      $key += 1;
    }
    // fix unmatch numeric bind key
    $is_param_type_exist = isset($param[1]);
    if ($is_param_type_exist) {
      $statement->bindParam($key, $param[0], $param[1]);
    } else {
      $statement->bindParam($key, $param[0]);
    }
  }
  try {
    $statement->execute();
  } catch (PDOException $error) {
    if ($sql_transaction_dept !== 0) {
      $pdo->rollBack();
    }
    send_json(500, [
      "sql_query" => $sql_query,
      "error" => $error,
    ]);
    throw $error;
  }
  return $statement;
}

function create_table($table_name, $table_properties)
{
  return execute_sql("
    CREATE TABLE {$table_name} (
      {$table_properties}
    ) ENGINE=InnoDB CHARACTER SET=utf8mb4;
  ");
}

/**
 * use with execute_sql function only.
 */
function use_sql_transaction($transaction_function) {
  global $pdo;
  global $sql_transaction_dept;
  if (++$sql_transaction_dept === 1) {
    $pdo->beginTransaction();
  }
  $transaction_result = $transaction_function();
  if (--$sql_transaction_dept === 0) {
    $pdo->commit();
  }
  return $transaction_result;
}

function __bindable_params($params) {
  $bindable_params = [];
  foreach ($params as $key => $value) {
    $bindable_params[":{$key}"] = $value;
  }
  return $bindable_params;
}

/**
 * BECAREFUL!!
 * the keys in $data parameter and the $table_name parameter
 * are inserted DIRECTLY to sql query.
 * Never let users write that for you.
 */
function execute_insert_sql($table_name, $data) {
  global $pdo;
  $keys = array_keys($data);
  $key_params = join(",", $keys);
  $value_params = join(",", array_map(
    function ($key) {return ":{$key}";},
    $keys
  ));
  $params = __bindable_params($data);
  execute_sql("
    INSERT INTO {$table_name} ({$key_params}) VALUES ({$value_params});
  ", $params);
  return $pdo->lastInsertId();
}

/**
 * BECAREFUL!!
 * the keys in $data parameter and the $table_name parameter
 * are inserted DIRECTLY to sql query.
 * Never let users write that for you.
 */
function execute_update_sql($table_name, $data, $where_clause) {
  $set_keys = array_keys($data);
  $where_clause_keys = array_keys($where_clause);

  foreach ($where_clause_keys as $key) {
    if (in_array($key, $set_keys)) {
      return send_json(400, [
        "msg" => "conflicted key",
      ]);
    }
  }

  $set_params = join(",", array_map(
    function ($key) {
      return "{$key} = COALESCE(:{$key}, {$key})";
    },
    $set_keys
  ));

  $where_clause_params = join(" AND ", array_map(
    function ($key) {
      return "{$key} = :{$key}";
    },
    $where_clause_keys
  ));

  $params = __bindable_params(
    array_merge($data, $where_clause)
  );

  execute_sql("
    UPDATE {$table_name}
    SET {$set_params}
    WHERE {$where_clause_params};
  ", $params);

  return $data;
}

function execute_delete_sql($table_name, $where_clause) {
  $where_clause_keys = array_keys($where_clause);

  $where_clause_params = join(" AND ", array_map(
    function ($key) {
      return "{$key} = :{$key}";
    },
    $where_clause_keys
  ));

  $params = __bindable_params(array_merge($where_clause));

  $result = execute_sql("
    DELETE FROM {$table_name}
    WHERE {$where_clause_params};
  ", $params);

  return $result;
}

function is_table_exist($table_name) {
  return execute_sql("
    SHOW TABLES LIKE '{$table_name}'
  ")->rowCount() !== 0;
}
