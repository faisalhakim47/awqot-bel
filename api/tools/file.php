<?php

function delete_file($filepath) {
  if (!file_exists($filepath)) {
    return true;
  }
  return unlink($filepath);
}

function get_file_as_base64($path) {
  $finfo = finfo_open(FILEINFO_MIME_TYPE);
  $mime_type = finfo_file($finfo, $path);
  finfo_close($finfo);
  $data = file_get_contents($path);
  $base64 = "data:" . $mime_type . ";base64," . base64_encode($data);
  return $base64;
}
