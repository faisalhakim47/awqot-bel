<?php

require_once __DIR__ . "/database.php";
require_once __DIR__ . "/configuration.php";

$__alter_functions = [];

function __get_current_structure_version() {
  if (is_table_exist("configurations")) {
    $version = execute_sql("
      SELECT value
      FROM configurations
      WHERE name = 'version'
    ")->fetch();
    if (isset($version["value"])) {
      return (int) $version["value"];
    } else {
      return 0;
    }
  } else {
    return -1;
  }
}

function alter_structure($alter_function) {
  global $__alter_functions;
  array_push($__alter_functions, function () use ($alter_function) {
    use_sql_transaction($alter_function);
  });
}

function ensure_upgrade() {
  global $__alter_functions;

  $latest_version = count($__alter_functions);
  $current_version = __get_current_structure_version();

  if ($current_version <= 0) {
    array_unshift($__alter_functions, function () use ($latest_version) {
      set_configuration("version", "" . $latest_version);
    });
    if ($current_version === -1) {
      array_unshift($__alter_functions, function () {
        create_table("configurations", "
          name VARCHAR(64) NOT NULL,
          value TEXT NOT NULL,
          PRIMARY KEY(name)
        ");
      });
    }
  } else {
    $__alter_functions = array_slice($__alter_functions, $current_version);
  }

  use_sql_transaction(function () use ($latest_version) {
    global $__alter_functions;

    foreach ($__alter_functions as $alter_function) {
      $alter_function();
    }

    set_configuration("version", "" . $latest_version);
  });

  return true;
}
