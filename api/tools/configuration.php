<?php

require_once __DIR__ . "/database.php";

function is_configuration_exist($name) {
  $configuration = execute_sql("
    SELECT configurations.value AS value
    FROM configurations
    WHERE configurations.name = :configuration_name
  ", [
    ":configuration_name" => $name,
  ])->fetch();
  return !!$configuration;
}

function get_configuration($name) {
  $configuration = execute_sql("
    SELECT configurations.value AS value
    FROM configurations
    WHERE configurations.name = :configuration_name
  ", [
    ":configuration_name" => $name,
  ])->fetch();
  return $configuration
    ? $configuration["value"]
    : null;
}

function set_configuration($name, $value) {
  if (is_configuration_exist($name)) {
    execute_update_sql("configurations", [
      "value" => [$value, PDO::PARAM_STR],
    ], [
      "name" => [$name, PDO::PARAM_STR],
    ]);
  } else {
    execute_insert_sql("configurations", [
      "value" => [$value, PDO::PARAM_STR],
      "name" => [$name, PDO::PARAM_STR],
    ]);
  }
}
