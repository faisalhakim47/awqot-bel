<?php

function mediainfo($filepath) {
  return array_reduce(
    array_filter(
      array_map(function ($info) {
        return explode(" : ", $info);
      }, explode("\n", shell_exec("mediainfo -f '" . $filepath . "'"))),
      function ($info) {
        return count($info) === 2;
      }
    ),
    function ($result, $info) {
      return $result + [
        trim($info[0]) => trim($info[1]),
      ];
    },
    []
  );
}
