<?php

function array_find($array, $check_fn) {
  foreach ($array as $item) {
    if ($check_fn($item)) return $item;
  }
}
