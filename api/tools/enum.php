<?php

function get_enum_values($table_name, $field_name) {
  $column_type = execute_sql("
    SHOW columns FROM {$table_name} WHERE Field = '{$field_name}'
  ")->fetch()["Type"];
  preg_match("/^enum\(\'(.*)\'\)$/", $column_type, $matches);
  $enum = explode("','", $matches[1]);
  return $enum;
}
