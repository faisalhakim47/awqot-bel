<?php

function is_online() {
  if (
    isset($_SERVER)
    && isset($_SERVER["HTTP_HOST"])
    && $_SERVER["HTTP_HOST"] === "192.168.1.1"
  ) return false;
  $connected = @fsockopen("google.com", 80, $errno, $errstr, 1); 
  if ($connected) {
    fclose($connected);
    return true;
  } else return false;
}
