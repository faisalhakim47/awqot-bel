<?php

require_once __DIR__ . "/tools/server.php";

$data = shell_exec("timedatectl");
$is_error = strpos($data, "I/O error") !== false
  || strpos($data, "RTC time: n/a") !== false;

send_json(200, [
  "is_error" => $is_error,
  "data" => $data,
]);
